FROM node:8.11.1 AS base
WORKDIR /app
ENV NODE_ENV production
COPY package*.json ./
RUN npm install
COPY server ./server
COPY common ./common
COPY images ./images
COPY dist ./dist
# COPY e2e ./e2e
COPY externalJS ./externalJS
COPY src ./src


FROM node:8.11.1-alpine AS release 
WORKDIR /app
ENV NODE_ENV production
COPY --from=base /app ./
CMD ["node", "server/server.js"]