'use strict';
var moment = require('moment');
var path = require('path');
const fs = require('fs');
const ejs = require('ejs');
const async = require('async');

const TAGS = require('../../server/service/tags');
const pushNotification = require('../../server/service/pushNotification');

module.exports = function (CompanyAnnouncement) {

  CompanyAnnouncement.list = function (symbol, callback) {
    const Companies = CompanyAnnouncement.app.models.Companies;

    Companies.find({
      'where': {
        'NSECode': symbol,
      },
      'include': {
        'relation': 'companyAnnouncements',
        'scope': {
          'where': {
            'source' : {
              'neq': 'PORTFOLIO',
            },
          },
          'order': 'announceTime desc',
          'limit': 5,
        },
      },
    }, function (err, data) {
      callback(err, data && data[0]);
    });
  };

  CompanyAnnouncement.remoteMethod('list', {
    http: {
      path: '/:symbol/announcements',
      verb: 'get',
    },
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: {
      arg: 'symbol',
      type: 'string',
    },
  });

  CompanyAnnouncement.observe('before save', function(ctx, next) {
    if (ctx.isNewInstance) {
      if (!ctx.instance.source !== 'PORTFOLIO') {
        Tags(ctx.instance, function(tag) {
          ctx.instance.tags = tag;
          next();
        });
      } else {
        next();
      }
    } else {
      next();
    }
  });

  CompanyAnnouncement.observe('after save', function(ctx, next) {
    const AnnouncementComprehend = CompanyAnnouncement.app.models.AnnouncementComprehend;
    if (ctx.isNewInstance && ctx.instance.source !== 'PORTFOLIO') {
      AnnouncementComprehend.emit('CreateComprehend', ctx.instance);
      var time = moment(ctx.instance.announceTime).format();
      if (moment().diff(time) <= (20 * 60 * 1000)) {
        console.log('Send Notification', ctx.instance.announceTime);
        CompanyAnnouncement.emit('SendAnnouncementEmail', ctx.instance);
      }
    }
    next();
  });

  CompanyAnnouncement.on('SendAnnouncementEmail', function (data) {
    const UserCompanies = CompanyAnnouncement.app.models.UserCompanies;
    const Email = CompanyAnnouncement.app.models.Email;
    const Subscribers = CompanyAnnouncement.app.models.Subscribers;

    UserCompanies.find({
      where: {
        companyId: data.companyId,
        notify: {
          'inq': data.tags,
        },
      },
      include: [{ relation: 'users', scope: { include: 'portfolios', }, }, 'companies',],
    }, function (err, users) {
      if (!err && users && users.length) {
        const details = users[0].toJSON();

        const emails = [];
        users.map(data => emails.push(data.toJSON().users.email));
        Subscribers.find({
          where: {
            email: {
              inq: emails,
            },
          },
        }, function (err, subscribers) {
          if (!err && subscribers && subscribers.length) {
            const notification = {
              'title': `${details.companies.name} - Announcement Notification`,
              'body': `Check your inbox for latest announcement for ${details.companies.name}`,
            };

            const endpoints = [];
            subscribers.map(data => data.toJSON().pushMessageData.map(endpoint => endpoints.push(endpoint)));
            pushNotification(endpoints, notification);
          }
        });

        const option = {
          company: details.companies.name,
          time: moment.utc(data.announceTime).utcOffset(330).format('LLLL'),
          source: data.source,
          url: data.docLink,
          title: data.title,
          description: data.description,
          price: details.companies.price,
        };

        const notifyTemplatePath = path.resolve(__dirname, '../../server/app/views/announcementNotify.ejs');
        const compiled = ejs.compile(fs.readFileSync(notifyTemplatePath, 'utf8'));

        users.map((user) => {

          user = user.toJSON();
          option.hasPortfolio = null;
          option.avgPrice = 0;
          option.netShares = 0;
          option.change = 0;

          const portfolio = user.users.portfolios;

          if (portfolio.length) {
            portfolio.map((por) => {
              if (por.companyId.toString() === data.companyId.toString()) {
                option.hasPortfolio = true;
                const buy = por.buy ? 1 : -1;
                option.avgPrice += por.transaction_price * por.transaction_shares * buy;
                option.netShares += por.transaction_shares * buy;
                option.change = 100 * (details.companies.price * option.netShares - option.avgPrice) / option.avgPrice;
                option.change = +option.change.toFixed(2);
              }
            });
          }

          option.avgPrice = option.avgPrice / option.netShares;
          option.avgPrice = +option.avgPrice.toFixed(2);

          const html = compiled(option);
          const options = {
            to: user.users.email,
            from: 'InvestLoop <no-reply@appgambit.com>',
            subject: `${details.companies.name} - Announcement Notification`,
            html: html,
          };


          Email.send(options, function (err, mail) { });

        });

        //  const toEmails = users.map((user) => {
        //   user = user.toJSON();
        //   return user.users.email;
        // });


      }
    });
  });
};

function Tags(str, NEXT) {
  const tagArray = TAGS.get();

  const tags = ['All',];
  tags.push(str.source);
  str = str.title + ' ' + str.description;
  str = str.toLowerCase();

  async.eachSeries(tagArray, function (data, cb) {
    let i = 0;
    async.each(data.fields, function (field, callback) {
      if (str.includes(field)) {
        i = 1;
      }
      callback();
    }, function () {
      if (i === 1) {
        tags.push(data.tag);
      }
      cb();
    });
  }, function () {
    NEXT(tags);
  });

}
