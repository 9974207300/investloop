'use strict';

var async = require('async');
var path = require('path');
const fs = require('fs');
const ejs = require('ejs');

// var mime = require('mime');
// var multer = require('multer');

// var storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, '../../src/assets/images')
//     },
//     filename: function (req, file, cb) {
//         cb(null, file.fieldname + '-' + Date.now() + '.' + mime.getExtension(file.mimetype))
//     }
// });

var ObjectId = require('mongodb').ObjectId;
module.exports = function (Users) {
  // Users.observe('before save', function(ctx, next){
  //   var id = ctx.options;
  //   return next()
  // });
  Users.contactAdmin = function (id, company, question, callback) {
    const Email = Users.app.models.Email;

    Users.find({
      where: {
        id: id,
      },
    }, function(err, user){
      if (err) return callback(err, null);
      console.log(user);
      const askAdminQuestionTemplate = path.resolve(__dirname, '../../server/app/views/askQuestion.ejs');
      const compiled = ejs.compile(fs.readFileSync(askAdminQuestionTemplate, 'utf8'));
      const html = compiled({ company: company, user: user[0].email, question: question.question, description: question.description, });
  
      const options = {
        to: 'dhaval@jumpbyte.com',
        from: 'InvestLoop <no-reply@appgambit.com>',
        subject: 'Users Question',
        html: html,
      };
  
      Email.send(options, function (err, mail) { 
        callback(err, mail);
      });
    });
  };

  Users.remoteMethod('contactAdmin', {
    http: {
      path: '/:id/contactAdmin',
      verb: 'post',
    },
    returns: {
      arg: 'success',
      type: 'object',
    },
    accepts: [
      { arg: 'id', type: 'string', },
      { arg: 'company', type: 'string', },
      { arg: 'question', type: 'object', http: { source: 'body', },  },
    ],
  });

  Users.remoteMethod('totalShares', {
    http: {
      path: '/:id/portfolios/:company_name/totalShares',
      verb: 'get',
    },
    returns: {
      arg: 'shares',
      type: 'number',
    },
    accepts: [{
      arg: 'company_name',
      type: 'string',
    },
    {
      arg: 'id',
      type: 'string',
    },],
  });

  Users.totalShares = function (company_name, userId, callback) {
    const Companies = Users.app.models.Companies;
    const Portfolio = Users.app.models.Portfolio;
    Companies.find({
      where: {
        name: company_name,
      },
    }, function (err, data) {

      if (err) return callback(err, null);
      if (!data || !data[0]) return callback(null, 0);
      data = data[0];
      Portfolio.find({
        where: {
          userId: userId,
          companyId: data.id,
        },
      }, function (err, res) {

        if (err) return callback(err, null);
        if (res && res.length) {
          let shares = 0;
          async.each(res, function (portfolios, cb) {
            if (portfolios.buy) {
              shares += portfolios.transaction_shares;
            } else {
              shares -= portfolios.transaction_shares;
            }
            cb();
          }, function () {
            callback(null, shares);
          });
        } else {
          callback(null, 0);
        }
      });
    });
  };

  Users.isVerified = function (email, callback) {
    Users.find({
      where: {
        email: email,
      },
    }, function (err, user) {
      if (err) {
        callback(err, null);
      } else {
        if (user[0].emailVerified) {
          callback(null, true);
        } else {
          callback(null, false);
        }
      }
    });
  };

  Users.remoteMethod('isVerified', {
    http: {
      path: '/:email/isVerified',
      verb: 'get',
    },
    returns: {
      arg: 'verified',
      type: 'boolean',
    },
    accepts: {
      arg: 'email',
      type: 'string',
    },
  });

  Users.emailExists = function (email, callback) {
    Users.find({
      where: {
        email: email,
      },
    }, function (err, user) {
      if (err) {
        callback(err, null);
      } else {
        if (user[0]) {
          callback(null, 'true');
        } else {
          callback(null, 'false');
        }
      }
    });
  };

  Users.remoteMethod('emailExists', {
    http: {
      path: '/:email/emailExists',
      verb: 'get',
    },
    returns: {
      arg: 'exists',
      type: 'string',
    },
    accepts: {
      arg: 'email',
      type: 'string',
    },
  });

  Users.afterRemote('create', function (context, user, next) {
    var options = {
      host: process.env.APP_URL,
      port: process.env.APP_PORT,
      type: 'email',
      to: user.email,
      from: 'InvestLoop <no-reply@appgambit.com>',
      subject: 'Email Verification',
      template: path.resolve(__dirname, '../../server/app/views/VerifyEmailTemplate.ejs'),
      memberEmail: user.email,
      text: '{href}',
      redirect: '/verified',
      user: user,
    };

    user.verify(options, function (err, response) {
      if (err) {
        Users.deleteById(user.id);
        return next(err);
      }
      next();
    });
  });

  Users.on('resetPasswordRequest', function (info) {
    const Email = Users.app.models.Email;
    const url = `http://${process.env.APP_URL}:${process.env.APP_PORT}/reset-password?token=${info.accessToken.id}`;

    const resetPassowordTemplatePath = path.resolve(__dirname, '../../server/app/views/forgotPassword.ejs');
    const compiled = ejs.compile(fs.readFileSync(resetPassowordTemplatePath, 'utf8'));
    const html = compiled({ text: url, memberEmail: info.user.email, });
    const options = {
      to: info.user.email,
      from: 'InvestLoop <no-reply@appgambit.com>',
      subject: 'Reset Password',
      html: html,
    };

    Email.send(options, function (err, mail) { });
  });

  Users.afterRemote('setPassword', function (context, info, next) {
    const AccessToken = Users.app.models.AccessToken;
    const id = context.req.accessToken.id;
    AccessToken.deleteById(id);
    return next();
  });

  // Users.observe('before save', function(ctx, next){
  //   var upload = multer({ storage: storage }).single('file');
  //   console.log(ctx);
  // });

  Users.observe('after save', function (ctx, next) {
    var userDetails = ctx.instance;

    const Role = Users.app.models.Role;
    const RoleMapping = Users.app.models.RoleMapping;
    RoleMapping.defineProperty('principalId', { type: ObjectId, });

    Role.findOne({
      name: 'user',
    }, ctx.options, function (err, role) {
      if (err) {
        next(err);
        return;
      }
      role.principals.create({
        principalType: RoleMapping.USER,
        principalId: userDetails.id,
      }, ctx.options, function () { });
      return next();
    });

  });

  Users.observe('before delete', function (ctx, next) {
    const UserCompanies = Users.app.models.UserCompanies;

    UserCompanies.find({
      where: {
        userId: ctx.where.id,
      },
    }, ctx.options, function (err, companies) {
      async.eachSeries(companies, function (company, callback) {

        company.destroy(function () {
          callback();
        });

      }, function (err) {
        if (err) {
          return next(err);
        }
        return next();
      });

    });

  });
};
