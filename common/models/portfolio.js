'use strict';
const moment = require('moment');
var async = require('async');
var historicalPrice = require('../../server/service/historicalPrice');

module.exports = function (Portfolio) {

  Portfolio.observe('before save', function (ctx, next) {
    if (ctx.isNewInstance) {
      ctx.created_on = moment.utc().format();
      next();
    } else {
      ctx.hookState.companyId = ctx.currentInstance.companyId;
      ctx.hookState.transaction_date = ctx.currentInstance.transaction_date;
      next();
    }
  });

  Portfolio.observe('after save', function (ctx, next) {
    const UserCompanies = Portfolio.app.models.UserCompanies;
    const CompanyAnnouncement = Portfolio.app.models.CompanyAnnouncement;
    const Companies = Portfolio.app.models.Companies;

    var description = '';
    if (ctx.instance.buy) {
      description = `You bought ${ctx.instance.transaction_shares} shares of %name% for Rs.${ctx.instance.transaction_price}`;
    } else {
      description = `You sold ${ctx.instance.transaction_shares} shares of %name% for Rs.${ctx.instance.transaction_price}`;
    }

    const companyId = ctx.instance.companyId;
    const userId = ctx.instance.userId;

    if (ctx.isNewInstance) {
      var information = {
        companyId: companyId,
        source: 'PORTFOLIO',
        title: ctx.instance.buy ? 'Shares Bought' : 'Shares Sold',
        description: description,
        announceTime: moment.utc(ctx.instance.transaction_date).format(),
        createTime: moment.utc().format(),
        userId: userId,
        portfolioId: ctx.instance.id,
        announcePrice: 0,
      };

      function addUserCompany(callback) {
        UserCompanies.findOrCreate({
          companyId: companyId,
          userId: userId,
          public: false,
        }, function (err, company) {
          callback(null, 'done');
        });
      }

      function addCompanyAnnouncement(callback) {
        Companies.find({
          where: {
            id: companyId,
          },
        }, function (err, company) {
          information.description = information.description.replace('%name%', company[0].name);
          historicalPrice({
            symbol: company[0].BSESymbol,
            date: moment(ctx.instance.transaction_date).format('Y-MM-D'),
          }, function (err, price) {
            if (price) {
              information.announcePrice = price.close;
            }

            CompanyAnnouncement.create(information, function (err, data) {
              callback(null, 'done');
            });
          });
        });
      }

      async.parallel([
        addCompanyAnnouncement,
        addUserCompany,
      ], function () {
        next();
      });

    } else {

      var information = {
        companyId: companyId,
        title: ctx.instance.buy ? 'Shares Bought' : 'Shares Sold',
        description: description,
        announceTime: moment.utc(ctx.instance.transaction_date).format(),
      };

      if (ctx.hookState.companyId !== companyId || ctx.hookState.transaction_date !== ctx.instance.transaction_date) {
        Companies.find({
          where: {
            id: companyId,
          },
        }, function (err, company) {
          information.description = information.description.replace('%name%', company[0].name);
          historicalPrice({
            symbol: company[0].BSESymbol,
            date: moment(ctx.instance.transaction_date).format('Y-MM-D'),
          }, function (err, price) {
            if (price) {
              information.announcePrice = price.close;
            } else {
              information.announcePrice = 0;
            }

            CompanyAnnouncement.updateAll({
              portfolioId: ctx.instance.id,
            }, information, function (err, data) {
              next();
            });
          });
        });
      } else {
        CompanyAnnouncement.updateAll({
          portfolioId: ctx.instance.id,
        }, information, function(err, data){
          next();
        });
      }
    }
  });

  Portfolio.observe('after delete', function(ctx, next){
    const CompanyAnnouncement = Portfolio.app.models.CompanyAnnouncement;
    
    CompanyAnnouncement.destroyAll({
      portfolioId: ctx.where.id,
    }, function(err, data){
      next();
    });
  });
};
