'use strict';
const currentPrice = require('../../server/service/currentPrice');
const historicalPrice = require('../../server/service/historicalPrice');
const moment = require('moment');

module.exports = function (Usercompanies) {
  Usercompanies.observe('before save', function (ctx, next) {
    const Companies = Usercompanies.app.models.Companies;

    if (ctx.isNewInstance) {
      ctx.instance.createdOn = moment.utc().format();
      if (ctx.instance.public) {
        Companies.find({
          where: {
            id: ctx.instance.companyId,
          },
        }, function (err, company) {
          if (err) {
            next();
          } else {
            currentPrice(company[0].BSESymbol, function (err, price) {
              if (price) {
                ctx.instance.recommendedPrice = price.regularMarketPrice.raw || price.regularMarketPrice;
                next();
              } else {
                next();
              }
            });
          }
        });
      } else {
        next();
      }
    } else {
      if ((ctx.data.recommendedPrice !== ctx.currentInstance.recommendedPrice) && ctx.data.recommendedPrice !== 'undefined') {
        next();
      } else if ((ctx.data.createdOn !== ctx.currentInstance.createdOn) && ctx.data.createdOn !== 'undefined') {
        Companies.find({
          where: {
            id: ctx.currentInstance.companyId,
          },
        }, function (err, company) {
          if (company && company[0]) {
            historicalPrice({
              symbol: company[0].BSESymbol,
              date: moment(ctx.data.createdOn).format('Y-MM-DD'),
            }, function (err, price) {
              if (price) {
                ctx.data.recommendedPrice = price.close;
                next();
              } else {
                next('Price not found');
              }
            });
          } else {
            next('Company not found');
          }
        });
      } else {
        next();
      }
    }
  });

  Usercompanies.observe('after save', function (ctx, next) {
    if (ctx.isNewInstance) {
      var Usercompany = ctx.instance.toJSON();
      const TrackableCompanies = Usercompanies.app.models.TrackableCompanies;

      const filter = {
        where: {
          companyId: Usercompany.companyId,
        },
      };

      TrackableCompanies.findOne(filter, ctx.options, function (err, trackableCompany) {
        if (err) {
          return next(err);
        } else if (!trackableCompany) {
          TrackableCompanies.create({
            companyId: Usercompany.companyId,
          }, ctx.options, function (err, company) {
            if (err) {
              return next(err);
            }
            return next();
          });
        } else {
          trackableCompany.updateAttributes({
            userTracking: trackableCompany.userTracking + 1,
          }, ctx.options, function (err, company) {
            if (err) {
              return next(err);
            }
            return next();
          });
        }

      });
    } else {
      return next();
    }

  });

  Usercompanies.observe('before delete', function (ctx, next) {
    const TrackableCompanies = Usercompanies.app.models.TrackableCompanies;

    Usercompanies.findOne({
      where: {
        id: ctx.where.id,
      },
    }, ctx.options, function (err, userCompany) {
      if (err) {
        return next(err);
      }

      const filter = {
        where: {
          companyId: userCompany.companyId,
        },
      };

      TrackableCompanies.findOne(filter, function (err, trackableCompany) {
        if (err) {
          return next(err);
        }
        trackableCompany.updateAttributes({
          userTracking: trackableCompany.userTracking - 1,
        }, ctx.options, function (err, company) {
          if (err) {
            return next(err);
          }
          return next();
        });
      });
    });
  });

};
