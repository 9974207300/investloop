'use strict';
var moment = require('moment');
var q = require('q');
var async = require('async');

var BSEAnnoucements = require('../../server/service/BSEAnnouncements');
var historicalPrice = require('../../server/service/historicalPrice');
var currentPrice = require('../../server/service/currentPrice');
var NSEAnnouncements = require('../../server/service/NSEAnnouncements');
var corpActions = require('../../server/service/corporateAction');
var CompanyNews = require('../../server/service/companyNews');
var MCNews = require('../../server/service/moneyControl');

var GetBSEAnnouncements = function (ctx, CompanyAnnouncement) {
  BSEAnnoucements(ctx.instance.BSECode, (err, data) => {
    var defer = q.defer();

    if (err) {
      return defer.resolve();
    } else {
      async.eachSeries(data, function (info, cb) {
        var priceData = {
          symbol: ctx.instance.BSESymbol,
          date: moment(info.recievedTime).format('Y-MM-DD'),
        };

        historicalPrice(priceData, function (err, hPrice) {
          var information = {
            companyId: ctx.instance.companyId,
            source: info.source,
            title: info.heading,
            description: info.news,
            docLink: info.pdfLink,
            announceTime: moment.utc(info.recievedTime).format(),
            createTime: moment.utc().format(),
          };

          function createAnnouncement() {
            CompanyAnnouncement.findOrCreate({
              where: {
                title: information.title,
                description: information.description,
                docLink: information.docLink,
                announceTime: information.announceTime,
              },
            }, information, ctx.options, function (err, result) {
              cb();
            });
          }

          if (hPrice) {
            information.announcePrice = hPrice.close;
            createAnnouncement();
          } else {
            if (moment.utc().format('DD/MM/Y') === moment.utc(info.recievedTime).format('DD/MM/Y')) {
              currentPrice(priceData.symbol, function (err, cPrice) {
                if (cPrice) {
                  information.announcePrice = cPrice.regularMarketPrice;
                  createAnnouncement();
                } else {
                  information.announcePrice = 0;
                  createAnnouncement();
                }
              });
            } else {
              information.announcePrice = 0;
              createAnnouncement();
            }
          }
        });
      }, function(err) {
        return defer.resolve();
      });
    }
    return defer.promise;
  });
};

var GetNSEAnnouncements = function (ctx, CompanyAnnouncement) {
  NSEAnnouncements(ctx.instance.NSECode, function (err, data) {
    var defer = q.defer();

    if (err) {
      return defer.resolve();
    } else {
      async.eachSeries(data, function (info, cb) {
        var priceData = {
          symbol: ctx.instance.NSESymbol,
          date: moment(info.recievedTime).format('Y-MM-DD'),
        };

        historicalPrice(priceData, function (err, hPrice) {
          var information = {
            companyId: ctx.instance.companyId,
            source: info.source,
            title: info.heading,
            description: info.news,
            docLink: info.pdfLink,
            announceTime: moment.utc(info.recievedTime).format(),
            createTime: moment.utc().format(),
          };

          function createAnnouncement() {
            CompanyAnnouncement.findOrCreate({
              where: {
                title: information.title,
                description: information.description,
                docLink: information.docLink,
                announceTime: information.announceTime,
              },
            }, information, ctx.options, function (err, result) {
              cb();
            });
          }

          if (hPrice) {
            information.announcePrice = hPrice.close;
            createAnnouncement();
          } else {
            if (moment.utc().format('DD/MM/Y') === moment.utc(info.recievedTime).format('DD/MM/Y')) {
              currentPrice(priceData.symbol, function (err, cPrice) {
                if (cPrice) {
                  information.announcePrice = cPrice.regularMarketPrice;
                  createAnnouncement();
                } else {
                  information.announcePrice = 0;
                  createAnnouncement();
                }
              });
            } else {
              information.announcePrice = 0;
              createAnnouncement();
            }
          }
        });
      }, function (err) {
        return defer.resolve();
      });
    }
    return defer.promise;
  });
};

var Action = function (ctx, CompanyAnnouncement) {
  corpActions(ctx.instance.NSECode, function (err, data) {
    var defer = q.defer();

    if (err) {
      return defer.resolve();
    } else {
      async.eachSeries(data, function (info, cb) {
        var priceData = {
          symbol: ctx.instance.NSESymbol,
          date: moment(info.recievedTime).format('Y-MM-DD'),
        };

        historicalPrice(priceData, function (err, hPrice) {
          var information = {
            companyId: ctx.instance.companyId,
            source: info.source,
            title: info.heading,
            description: info.news,
            docLink: info.pdfLink,
            announceTime: moment.utc(info.recievedTime).format(),
            createTime: moment.utc().format(),
          };

          function createAnnouncement() {
            CompanyAnnouncement.findOrCreate({
              where: {
                title: information.title,
                description: information.description,
                docLink: information.docLink,
                announceTime: information.announceTime,
              },
            }, information, ctx.options, function (err, result) {
              cb();
            });
          }

          if (hPrice) {
            information.announcePrice = hPrice.close;
            createAnnouncement();
          } else {
            if (moment.utc().format('DD/MM/Y') === moment.utc(info.recievedTime).format('DD/MM/Y')) {
              currentPrice(priceData.symbol, function (err, cPrice) {
                if (cPrice) {
                  information.announcePrice = cPrice.regularMarketPrice;
                  createAnnouncement();
                } else {
                  information.announcePrice = 0;
                  createAnnouncement();
                }
              });
            } else {
              information.announcePrice = 0;
              createAnnouncement();
            }
          }
        });
      }, function (err) {
        return defer.resolve();
      });
    }
    return defer.promise;
  });
};

// var News = function(ctx, CompanyAnnouncement){
//   CompanyNews({
//     code: ctx.instance.BSECode,
//     date: moment().subtract(3, 'months').format()
//   }, function(err, data){
//     var defer = q.defer();
//     if(err){
//       return defer.resolve()
//     }
//     async.eachSeries(data, function(info, cb){
//       var priceData = {
//         symbol: ctx.instance.NSESymbol,
//         date: moment(info.recievedTime).format('Y-MM-DD')
//       }

//       historicalPrice(priceData, function (err, hPrice) {
//         var information = {
//           companyId: ctx.instance.companyId,
//           source: info.source,
//           title: info.heading,
//           description: info.news,
//           docLink: info.pdfLink,
//           announceTime: moment.utc(info.recievedTime).format(),
//           createTime: moment.utc().format(),
//         };

//         function createAnnouncement() {
//           CompanyAnnouncement.findOrCreate({
//             where: {
//               title: information.title,
//               description: information.description,
//               docLink: information.docLink,
//               // announceTime: information.announceTime
//             }
//           }, information, ctx.options, function (err, result) {
//             cb()
//           });
//         }

//         if (hPrice) {
//           information.announcePrice = hPrice.close;
//           createAnnouncement()
//         } else {
//           if (moment.utc().format('DD/MM/Y') === moment.utc(info.recievedTime).format('DD/MM/Y')) {
//             currentPrice(priceData.symbol, function (err, cPrice) {
//               if (cPrice) {
//                 information.announcePrice = cPrice.regularMarketPrice;
//                 createAnnouncement()
//               } else {
//                 information.announcePrice = 0;
//                 createAnnouncement()
//               }
//             });
//           } else {
//             information.announcePrice = 0;
//             createAnnouncement()
//           }
//         }
//       });
//     }, function(){
//       return defer.resolve()
//     });

//     return defer.promise
//   });
// }

var GETMCNews = function (ctx, CompanyAnnouncement) {
  MCNews(ctx.instance.BSECode, function (err, data) {
    var defer = q.defer();
    if (err) {
      return defer.resolve();
    }
    async.eachSeries(data, function (info, cb) {
      var priceData = {
        symbol: ctx.instance.NSESymbol,
        date: moment(info.recievedTime).format('Y-MM-DD'),
      };

      historicalPrice(priceData, function (err, hPrice) {
        var information = {
          companyId: ctx.instance.companyId,
          source: info.source,
          title: info.heading,
          description: info.news,
          docLink: info.pdfLink,
          announceTime: moment.utc(info.recievedTime).format(),
          createTime: moment.utc().format(),
        };

        function createAnnouncement() {
          CompanyAnnouncement.findOrCreate({
            where: {
              title: information.title,
              description: information.description,
              docLink: information.docLink,
              // announceTime: information.announceTime
            },
          }, information, ctx.options, function (err, result) {
            cb();
          });
        }

        if (hPrice) {
          information.announcePrice = hPrice.close;
          createAnnouncement();
        } else {
          if (moment.utc().format('DD/MM/Y') === moment.utc(info.recievedTime).format('DD/MM/Y')) {
            currentPrice(priceData.symbol, function (err, cPrice) {
              if (cPrice) {
                information.announcePrice = cPrice.regularMarketPrice;
                createAnnouncement();
              } else {
                information.announcePrice = 0;
                createAnnouncement();
              }
            });
          } else {
            information.announcePrice = 0;
            createAnnouncement();
          }
        }
      });
    }, function () {
      return defer.resolve();
    });

    return defer.promise;
  });
};

var CompanyCurrentPriceUpdate = function (ctx, Companies) {
  var defer = q.defer();
  currentPrice(ctx.instance.BSESymbol, function (err, price) {
    if (price) {
      Companies.updateAll({
        id: ctx.instance.companyId,
      }, {
        price: price.regularMarketPrice.raw || price.regularMarketPrice,
        previousPrice: price.regularMarketPreviousClose.raw || price.regularMarketPreviousClose,
      }, function () {
        defer.resolve();
      });
    } else {
      defer.resolve();
    }
  });
  return defer.promise;
};

// var CompanyPreviousPriceUpdate = function(ctx, Companies){
//   var defer = q.defer();
//   const data = {
//     symbol: ctx.instance.BSESymbol,
//     date: moment().subtract(1, 'd').format('Y-MM-DD')
//   };

//   historicalPrice(data, function(err, price){
//     if(price) {
//       Companies.updateAll({
//         id: ctx.instance.companyId
//       },
//       {
//         previousPrice: price.close
//       }, function(){
//         defer.resolve();
//       });
//     } else {
//       defer.resolve()
//     }
//   });
//   return defer.promise
// }

module.exports = function (TrackableCompanies) {
  TrackableCompanies.observe('before save', function (ctx, next) {
    if (ctx.currentInstance) {
      ctx.hookState.userTracking = ctx.currentInstance.userTracking;
    }

    if (ctx.isNewInstance) {
      const Companies = TrackableCompanies.app.models.Companies;
      const filter = {
        where: {
          id: ctx.instance.companyId,
        },
      };
      Companies.findOne(filter, ctx.options, function (err, company) {
        if (err) {
          return next(err);
        }

        ctx.instance.companyId = company.id;
        ctx.instance.userTracking = 1;
        ctx.instance.BSECode = company.BSECode;
        ctx.instance.NSECode = company.NSECode;
        ctx.instance.BSESymbol = company.BSESymbol;
        ctx.instance.NSESymbol = company.NSESymbol;
        ctx.instance.trackingInterval = 15;
        ctx.instance.lastTrackTime = moment.utc().format();
        ctx.instance.nextTrackTime = moment.utc().add(15, 'minutes').format();

        return next();
      });
    } else {
      return next();
    }
  });

  TrackableCompanies.observe('after save', function (ctx, next) {
    const CompanyAnnouncement = TrackableCompanies.app.models.CompanyAnnouncement;
    const Companies = TrackableCompanies.app.models.Companies;

    var reTrack = false;
    if (ctx.hookState && ctx.hookState.userTracking == 0 && ctx.instance.userTracking == 1) {
      reTrack = true;
    }

    if (ctx.isNewInstance || reTrack) {
      console.log('New Instance/ReTrack', reTrack);

      q.all([
        GetBSEAnnouncements(ctx, CompanyAnnouncement),
        GetNSEAnnouncements(ctx, CompanyAnnouncement),
        Action(ctx, CompanyAnnouncement),
        // News(ctx, CompanyAnnouncement),
        GETMCNews(ctx, CompanyAnnouncement),
        CompanyCurrentPriceUpdate(ctx, Companies),
        // CompanyPreviousPriceUpdate(ctx, Companies)
      ])
        .then(function () {
          next();
        });
    } else {
      return next();
    }
  });
};
