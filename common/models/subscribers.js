'use strict';

const webpush = require('web-push');
var moment = require('moment');
var path = require('path');
const fs = require('fs');
const ejs = require('ejs');
var jwt = require('jsonwebtoken');

const secret = 'fba6f160-4847-11e8-bc8d-d77abe552380';

module.exports = function (Subscribers) {
  Subscribers.push = function (email, pushData, callback) {
    Subscribers.updateAll({
      email: email,
    },
    {
      $push: {
        pushMessageData: pushData.data,
      },
    },
    { allowExtendedOperators: true, },
    function (err, subscriber) {
      callback(err, subscriber && subscriber.count > 0);
    });
  };

  Subscribers.remoteMethod('push', {
    http: {
      path: '/:email/push',
      verb: 'post',
    },
    returns: {
      arg: 'success',
      type: 'boolean',
    },
    accepts: [
      { arg: 'email', type: 'string', },
      { arg: 'pushData', type: 'object', http: { source: 'body', }, },
    ],
  });

  Subscribers.unsubscribe = function (id, callback) {
    Subscribers.destroyById(id, function (err, subscriber) {
      callback(err, true);
    });
  };

  Subscribers.remoteMethod('unsubscribe', {
    http: {
      path: '/:id/unsubscribe',
      verb: 'get',
    },
    returns: {
      arg: 'status',
      type: 'boolean',
    },
    accepts: {
      arg: 'id',
      type: 'string',
    },
  });

  Subscribers.decodeJWT = function (token, callback) {
    if (!token) {
      callback('Token not recieved', null);
    } else {
      const data = jwt.verify(token, secret);
      if (data.email) {
        callback(null, data.email);
      } else {
        callback('Invalid token', null);
      }
    }
  };

  Subscribers.remoteMethod('decodeJWT', {
    http: {
      path: '/:token/jwt',
      verb: 'get',
    },
    returns: {
      arg: 'email',
      type: 'string',
    },
    accepts: {
      arg: 'token',
      type: 'string',
    },
  });

  Subscribers.emailExists = function (email, callback) {
    Subscribers.find({
      where: {
        email: email,
      },
    }, function (err, user) {
      if (err) {
        callback(err, null);
      } else {
        if (user[0]) {
          callback(null, true);
        } else {
          callback(null, false);
        }
      }
    });
  };

  Subscribers.remoteMethod('emailExists', {
    http: {
      path: '/:email/exists',
      verb: 'get',
    },
    returns: {
      arg: 'exists',
      type: 'boolean',
    },
    accepts: {
      arg: 'email',
      type: 'string',
    },
  });

  Subscribers.verify = function (token, callback) {
    const data = jwt.verify(token, secret);
    const d = new Date();
    const now = Math.round(d.getTime() / 1000);
    var value = {};
    if (now > data.exp) {
      var exp = 7 * 24 * 60 * 60;
      var iat = Math.round(d.getTime() / 1000);
      value.verificationToken = jwt.sign({ email: data.email, iat: iat, }, secret, { expiresIn: exp, });
    } else {
      value = {
        verificationToken: null,
        verified: true,
      };
    }

    Subscribers.updateAll({
      email: data.email,
    },
    value,
    function (err, subscriber) {
      if (err) {
        callback(err, null);
      } else if (value.verificationToken && subscriber.count > 0) {
        const Email = Subscribers.app.models.Email;

        const url = `http://${process.env.APP_URL}:${process.env.APP_PORT}/api/Subscribers/verify?token=${value.verificationToken}`;
        const verifyTemplatePath = path.resolve(__dirname, '../../server/app/views/subscriberVerify.ejs');
        const compiled = ejs.compile(fs.readFileSync(verifyTemplatePath, 'utf8'));
        const html = compiled({ text: url, });
        const options = {
          to: data.email,
          from: 'InvestLoop <no-reply@appgambit.com>',
          subject: 'Email Verification',
          html: html,
        };

        Email.send(options, function (err, mail) {
          callback(null, `tokenExpired_${value.verificationToken}`);
        });
      } else {
        callback(null, subscriber.count > 0 ? `true_${token}` : 'false');
      }
    });

  };

  Subscribers.remoteMethod('verify', {
    http: {
      path: '/verify',
      verb: 'get',
    },
    returns: {
      arg: 'verified',
      type: 'string',
    },
    accepts: {
      arg: 'token',
      type: 'string',
    },
  });

  Subscribers.observe('before save', function (ctx, next) {
    if (ctx.isNewInstance) {
      const d = new Date();
      var exp = 7 * 24 * 60 * 60;
      var iat = Math.round(d.getTime() / 1000);
      ctx.instance.verificationToken = jwt.sign({ email: ctx.instance.email, iat: iat, }, secret, { expiresIn: exp, });
    }
    next();
  });

  Subscribers.afterRemote('create', function (ctx, subscriber, next) {
    const Email = Subscribers.app.models.Email;

    const url = `http://${process.env.APP_URL}:${process.env.APP_PORT}/api/Subscribers/verify?token=${subscriber.verificationToken}`;

    const verifyTemplatePath = path.resolve(__dirname, '../../server/app/views/subscriberVerify.ejs');
    const compiled = ejs.compile(fs.readFileSync(verifyTemplatePath, 'utf8'));
    const html = compiled({ text: url, memberEmail: subscriber.email, });
    const options = {
      to: subscriber.email,
      from: 'InvestLoop <no-reply@appgambit.com>',
      subject: 'Email Verification',
      html: html,
    };

    Email.send(options, function (err, mail) {
      next(err);
    });

  });

  Subscribers.afterRemote('verify', function (ctx, isVerified, next) {
    var url = `http://${process.env.APP_URL}:${process.env.APP_PORT}`;
    if (isVerified.verified.indexOf('true_') === 0) {
      const token = isVerified.verified.substring(5);
      url = `${url}?subscribed=${token}`;
    } else if (isVerified.verified.indexOf('tokenExp') === 0) {
      const token = isVerified.verified.substring(13);
      url = `${url}?expired=${token}`;
    }
    ctx.res.redirect(url);
  });

  Subscribers.afterRemote('unsubscribe', function (ctx, status, next) {
    const url = `http://${process.env.APP_URL}:${process.env.APP_PORT}`;
    ctx.res.redirect(url);
  });
};
