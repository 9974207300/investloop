'use strict';

module.exports = function (Alerts) {
  Alerts.observe('before save', function (ctx, next) {
    const TrackableCompanies = Alerts.app.models.TrackableCompanies;
    const UserCompanies = Alerts.app.models.UserCompanies;

    if (ctx.isNewInstance) {
      UserCompanies.find({
        where: {
          id: ctx.instance.userCompanyId,
        },
        include: 'companies',
      }, function (err, data) {
        if (err) {
          return next(err);
        } else {
          const company = data[0].toJSON();
          ctx.instance.createTimePrice = company.companies.price;
          TrackableCompanies.findOne({
            where: { companyId: company.companyId, },
          }, ctx.options, function (err, trackableCompany) {
            if (err) {
              return next(err);
            } else {
              trackableCompany.activeAlert = trackableCompany.activeAlert || 0;
              trackableCompany.updateAttributes({
                activeAlert: trackableCompany.activeAlert + 1,
              }, ctx.options, function (err) {
                return next(err);
              });
            }
          });
        }
      });
    } else {
      if (ctx.data.active === false || ctx.data.active === true) {
        const sign = ctx.data.active ? 1 : -1;
        Alerts.findOne({
          where: { id: ctx.where.id, },
          include: {
            'relation': 'userCompanies',
            'scope': {
              'include': {
                'relation': 'companies',
                'scope': {
                  'include': 'trackableCompanies',
                },
              },
            },
          },
        }, function (err, alert) {
          if (err) {
            return next();
          } else {
            alert = alert.toJSON();
            const trackableCompanies = alert.userCompanies.companies.trackableCompanies;

            TrackableCompanies.updateAll({
              id: trackableCompanies.id,
            }, {
              activeAlert: trackableCompanies.activeAlert + sign,
            }, function () {
              next();
            });
          }
        });
      } else {
        next();
      }

    }
  });

  Alerts.observe('before delete', function (ctx, next) {
    const TrackableCompanies = Alerts.app.models.TrackableCompanies;
    Alerts.findOne({
      where: { id: ctx.where.id, },
      include: {
        'relation': 'userCompanies',
        'scope': {
          'include': {
            'relation': 'companies',
            'scope': {
              'include': 'trackableCompanies',
            },
          },
        },
      },
    }, function (err, alert) {
      if (err) {
        return next();
      } else {
        if (alert.active) {
          alert = alert.toJSON();
          const trackableCompanies = alert.userCompanies.companies.trackableCompanies;
  
          TrackableCompanies.updateAll({
            id: trackableCompanies.id,
          }, {
            activeAlert: trackableCompanies.activeAlert - 1,
          }, function () {
            next();
          });
        } else {
          next();
        }
      }
    });
  });
};
