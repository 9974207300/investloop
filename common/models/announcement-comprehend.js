'use strict';

const async = require('async');

const keyPhrase = require('../../server/service/keyPhrases');
const sentiment = require('../../server/service/sentiment');
const entity = require('../../server/service/entity');

module.exports = function(Announcementcomprehend) {
  // Announcementcomprehend.on('CreateComprehend', function(data){
  //   var option = {
  //     announcementId: data.id
  //   };

  //   if (data.docLink) {
  //     const index1 = data.docLink.indexOf('//') + 2;
  //     const index2 = data.docLink.indexOf('/', index1);
  //     option.docLink_source =  data.docLink.substring(index1 , index2);
  //   }

  //   async.parallel([
  //     function(callback) {
  //       keyPhrase(data.title, function(err, title){
  //         option.title_key_phrase = title || [];
  //         callback();
  //       })
  //     },

  //     function(callback) {
  //       keyPhrase(data.description, function(err, des){
  //         option.description_key_phrase = des || [];
  //         callback();
  //       })
  //     },

  //     function(callback) {
  //       sentiment(data.title, function(err, title){
  //         option.title_sentiment = title || {};
  //         callback();
  //       })
  //     },

  //     function(callback) {
  //       sentiment(data.description, function(err, description){
  //         option.description_sentiment = description || {};
  //         callback();
  //       })
  //     },

  //     function(callback) {
  //       entity(data.title, function(err, title){
  //         option.title_entity = title || {};
  //         callback();
  //       })
  //     },

  //     function(callback) {
  //       entity(data.description, function(err, description){
  //         option.description_entity = description || {};
  //         callback();
  //       })
  //     }
  //   ], function(err, done){
  //     Announcementcomprehend.create(option);
  //   });
  // });

  Announcementcomprehend.observe('after save', function(ctx, next){
    const CompanyAnnouncement = Announcementcomprehend.app.models.CompanyAnnouncement;

    if (!ctx.isNewInstance) {
      CompanyAnnouncement.updateAll({
        id: ctx.instance.announcementId,
      },
      {
        verified: true,
      }, function(){
        next();
      });
    } else {
      next();
    }
  });
};
