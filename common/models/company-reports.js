'use strict';
const moment = require('moment');

module.exports = function (Companyreports) {
  Companyreports.observe('before save', function (ctx, next) {
    ctx.instance.userId = ctx.options.accessToken.userId;
    ctx.instance.createdOn = moment.utc().format();
    next();
  });
};
