'use strict';

module.exports = function(Companies) {
  Companies.observe('before save', function(ctx, next){
    if(ctx.isNewInstance) {
      ctx.instance.BSESymbol = ctx.instance.NSECode + '.BO';
      ctx.instance.NSESymbol = ctx.instance.NSECode + '.NS';
      next();
    } else {
      next();
    }
  });
};
