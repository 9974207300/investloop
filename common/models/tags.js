'use strict';

const TAGS = require('../../server/service/tags');

module.exports = function (Tags) {
  
  Tags.observe('after save', function (ctx, next) {
    TAGS.set(ctx.instance, ctx.isNewInstance);
    next();
  });

  Tags.observe('after delete', function (ctx, next) {
    const id = ctx.where.id;
    TAGS.delete(id);
    next();
  });
};
