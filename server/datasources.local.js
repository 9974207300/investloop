module.exports = {
  'db': {
    'host': process.env.DATASOURCE_URI,
    'port': 27017,
    'url': '',
    'database': 'mongoDS',
    'password': '',
    'name': 'db',
    'user': '',
    'connector': 'mongodb',
  },
  'InvestLoopEmailDS': {
    'name': 'InvestLoopEmailDS',
    'connector': 'mail',
    'transports': [
      {
        'type': 'smtp',
        'host': process.env.SMTP_SERVER,
        'secure': true,
        'port': process.env.SMTP_PORT,
        'tls': {
          'rejectUnauthorized': false,
        },
        'auth': {
          'user': process.env.SMTP_USERNAME,
          'pass': process.env.SMTP_PASSWORD,
        },
      },
    ],
  },
};
