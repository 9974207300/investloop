'use strict';

var request = require('request');
var moment = require('moment');
var async = require('async');

module.exports = function GetCompanyNews(data, callback) {
  var options = {
    method: 'GET',
    url: 'https://finance.google.com/finance/company_news',
    qs: {
      q: data.code, 
      startdate: moment(data.date).format('Y-M-DD'),
      enddate: moment().add(1, 'd').format('Y-M-DD'),
      output: 'JSON',
    },
  };
  request(options, function (err, res, body) {
    if(err){
      return callback(true, []);
    }
    try {
      const data = JSON.parse(body);
      var newsData = [];
  
      async.eachSeries(data.clusters, function (news, cb) {
        if (news.a) {
          news = news.a[0];
  
          if (news.d.indexOf('second') != -1) {
            news.d = moment.utc().subtract(news.d.substring(0, 2).trim(), 's').format();
          } else if (news.d.indexOf('minute') != -1) {
            news.d = moment.utc().subtract(news.d.substring(0, 2).trim(), 'm').format();
          } else if (news.d.indexOf('hour') != -1) {
            news.d = moment.utc().subtract(news.d.substring(0, 2).trim(), 'h').format();
          } else if (news.d.indexOf('includesday') != -1) {
            news.d = moment.utc().subtract(news.d.substring(0, 2).trim(), 'd').format();
          } else {
            news.d = moment.utc(news.d, ['MMM D, Y',]).format();
          }
  
          var info = {
            date: moment.utc(news.d).format(),
            recievedTime: news.d,
            heading: news.t.replace('&#39;', '\'').replace('&amp;', '&'),
            news: news.sp.replace('&#39;', '\'').replace('&amp;', '&'),
            source: 'GOOGLE FINANCE',
            pdfLink: news.u,
          };
          newsData.push(info);
          cb();
        } else {
          cb();
        }
  
      }, function () {
        return callback(null, newsData);
      });
    } catch (err) {
      return callback(true, []);
    }
  });
};
