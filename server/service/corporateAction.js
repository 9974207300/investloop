'use strict';

var request = require('cloudscraper');
var cheerio = require('cheerio');
var q = require('q');
var moment = require('moment');

module.exports = function GetNSEAnnouncements(code, cb) {
  var url ='https://www.nseindia.com/marketinfo/companyTracker/corpAction.jsp?symbol='+code;
  var Actiondata = [];
  var corpAction = [];
  request.get(url, function (error, response, body) {
    if (error){
      return cb(true, []);
    }
    try {
      var index = body.indexOf('<tr valign=top>');
      
      while(index != -1){
        var endIndex = body.indexOf('</tr>', index);
        var str = '<table>'+body.substring(index, (endIndex+5))+'</table>';
        Actiondata.push(str);
        index = body.indexOf('<tr valign=top>', endIndex);
      }
      
      var promise = Actiondata.map((data) => {
        var defer = q.defer();
        var $ = cheerio.load(data);
        var text = $('td').text().trim();
      
        var heading = null;
        if(data.indexOf('PER SHARE') != -1){
          heading = 'Dividend';
        } else if(data.indexOf('BONUS') != -1) {
          heading = 'Bonus';
        }
      
        if(heading){
          var xyz = {
            date: text.substring(0, 11).trim(),
            recievedTime: moment.utc(text.substring(0, 11).trim(), ['DD-MMM-Y',]).format(),
            heading:heading.replace('&#39;', '\'').replace('&amp;', '&'),
            news: text.substr(13).trim().replace('&#39;', '\'').replace('&amp;', '&'),
            source: 'NSE',
            pdfLink: '',
          };
        
          corpAction.push(xyz);
        }
        defer.resolve();
        return defer.promise;
      });
      
      q.all(promise)
        .then(function(){
          cb(null, corpAction);
        });
    } catch(err) {
      return cb(true, []);
    }
  });
};
