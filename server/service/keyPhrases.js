// 'use-strict';

// var aws = require('aws-sdk');
// var _ = require('lodash');

// aws.config.update({
//   accessKeyId: process.env.AWS_ACCESS_KEY,
//   secretAccessKey: process.env.AWS_SECRET_KEY,
//   region: process.env.AWS_REGION
// });

// var comprehend = new aws.Comprehend();

// const notAKeyPhrase = [
//   'ns', 'bs', 'bom', 'rs', 'rs.', 'per share'
// ];

// module.exports = function GetKeyPhrases(str, cb) {
//   var params = {
//     LanguageCode: 'en',
//     Text: str
//   };
//   comprehend.detectKeyPhrases(params, function (err, data) {
//     if (err) {
//       cb(err, null);
//     } else {
//       var result = [];

//       result = _.map(data.KeyPhrases, 'Text');
//       result = _.uniq(result);
//       result = _.remove(result, function (x) {
//         return notAKeyPhrase.indexOf(x.toLowerCase()) === -1 && x.length !== 1
//       });

//       cb(null, result);
//     }
//   });
// };