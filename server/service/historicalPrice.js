'use-strict';

var yahooFinance = require('yahoo-finance');
var moment = require('moment');

module.exports = function GetHistoricalPrice(data, cb) {
  var day = data.date;
  var i = 1;
  try {
    if (data.symbol == '.NS' || data.symbol == '.BO') {
      cb(true, null);
    } else {
      function Price() {
        yahooFinance.historical({
          symbol: data.symbol,
          from: day,
          to: day,
        }, function (err, quotes) {
          if (quotes && quotes[0] && quotes[0].close) {
            return cb(null, quotes[0]);
          } else {
            if (i == 8) {
              return cb(true, null);
            }
            day = moment(day, ['Y-MM-DD',]).subtract(1, 'd').format('Y-MM-DD');
            i++;
            Price();
          }
        });
      }

      Price();
    }
  } catch (err) {
    cb(true, null);
  }

};