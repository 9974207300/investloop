'use strict';

const async = require('async');
const path = require('path');
const fs = require('fs');
const ejs = require('ejs');

var app = require('../server');
const pushNotification = require('./pushNotification');

const UserCompanies = app.models.UserCompanies;
const Email = app.models.Email;
const Alerts = app.models.Alerts;
const Subscribers = app.models.Subscribers;

module.exports = function Alerts(trackableCompany, price) {
  UserCompanies.find({
    where: {
      companyId: trackableCompany.companyId,
    },
    'include': ['alerts', 'users', 'companies',],
  }, function (err, cmpny) {
    if (err) {
      console.log('alert err', err);
    } else if (cmpny && cmpny.length) {

      async.eachSeries(cmpny, function (company, callback) {
        company = company.toJSON();
        const alerts = company.alerts;
        const user = company.users;
        async.eachSeries(alerts, function (alert, cb) {

          if (alert.active) {
            if (alert.createTimePrice <= alert.alertPrice && price >= alert.alertPrice) {
              sendEmail(company.companies, user, alert);
              sendNotification(company.companies, user, alert);
            } else if (alert.createTimePrice > alert.alertPrice && price <= alert.alertPrice) {
              sendEmail(company.companies, user, alert);
              sendNotification(company.companies, user, alert);
            }
          }
          cb();
        }, function () {
          callback();
        });
      });
    }
  });
};

function sendEmail(company, user, alert) {
  const alertTemplatePath = path.resolve(__dirname, '../app/views/priceAlert.ejs');
  const compiled = ejs.compile(fs.readFileSync(alertTemplatePath, 'utf8'));
  const html = compiled({
    name: company.name,
    alertPrice: alert.alertPrice,
    price: company.price,
  });

  const options = {
    to: user.email,
    from: 'InvestLoop <no-reply@appgambit.com>',
    subject: `${company.name} - Price Alert`,
    html: html,
  };

  Email.send(options, function (err, mail) {
    if (err) {
      console.log('email error', err);
    } else if (mail) {
      Alerts.updateAll({
        id: alert.id,
      }, {
        active: false,
      }, function () {
        console.log('Alert sent', user.email, alert.id);
      });
    }
  });
}

function sendNotification(company, user, alert) {
  Subscribers.find({
    where: {
      email: user.email,
    },
  }, function (err, subscribers) {
    if (!err && subscribers && subscribers.length) {
      const notification = {
        'title': `${company.name} - Price Alert`,
        'body': `${company.name} has crossed price Rs.${alert.alertPrice}`,
      };

      const endpoints = [];
      subscribers.map(data => data.toJSON().pushMessageData.map(endpoint => endpoints.push(endpoint)));
      pushNotification(endpoints, notification);
    }
  });
}