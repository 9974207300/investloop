var request = require('request');
var cheerio = require('cheerio');
var q = require('q');
var moment = require('moment');

module.exports = function GetMCAnnouncements(code, cb) {

  var url = `https://www.moneycontrol.com/mccode/common/autosuggesion.php?query=${code}&type=1&format=json`;

  request(url, function (err, res, mcData) {
    if (err) {
      return cb(true, []);
    }

    mcData = JSON.parse(mcData);
    url = `https://www.moneycontrol.com/stocks/company_info/stock_news.php?sc_id=${mcData[0].sc_id}&durationType=M&duration=1`;

    var index = [];
    var news = [];
    var xyz = [];

    request(url, function (err, res, body) {
      var i = body.indexOf('<div class="MT15 PT10 PB10" style="border-bottom:1px #bfbfbf solid;">');
      
      while (i != -1) {
        index.push(i);
        i = body.indexOf('<div class="MT15 PT10 PB10" style="border-bottom:1px #bfbfbf solid;">', (i + 1));
      }

      news = index.map((i, ind) => {

        var last;
        if (ind == (index.length - 1)) {
          last = body.indexOf('</div><div class="CL"></div>', i);
        } else {
          last = index[ind + 1];
        }
        
        return body.substring(i, last);

      });

      var promise = news.map(function (info) {
        var zone = '+05:30';

        var defer = q.defer();     
        var $ = cheerio.load(info);

        var dt = $('.FL').children('p').first().text().trim();
        dt = dt.substr(0, dt.indexOf('Source')).replace(/\|/gi, '').trim();
        const link = $('.FL').children('a')['0'];
        const data = {
          date: dt,
          recievedTime: moment(dt + zone, 'h.mm a DD MMM YZ').utc().format(),
          heading: $('.FL').children('a').text().trim(),
          news: $('.FL').children('p').last().text().trim(),
          source: 'MONEY CONTROL',
          pdfLink: link && link.attribs ? `https://www.moneycontrol.com${link.attribs.href}` : '',
        };

        xyz.push(data);
        defer.resolve();
        return defer.promise;
      });

      q.all(promise)
        .then(function () {
          cb(null, xyz);
        })
        .catch(function(err){
          cb(err, []);
        });

    });
  });

};