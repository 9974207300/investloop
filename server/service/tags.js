'use-strict';

var TagArray = [];

var setTag = function(tag, isNew) {
  if (isNew) {
    TagArray.push(tag);
  } else {
    TagArray = TagArray.map((data) => {
      data = (data.id === tag.id) ? tag : data;
      return data;
    });
  }
};

var getTag = function() {
  return TagArray;
};

var deleteTag = function (id) {
  TagArray = TagArray.filter((data) => {
    return data.id.toString() !== id.toString();
  });
};

var createTag = function(data) {
  TagArray = data;
};

module.exports = {
  set: setTag,
  get: getTag,
  delete: deleteTag,
  create: createTag,
};