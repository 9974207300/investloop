'use strict';
var moment = require('moment');
var q = require('q');
var async = require('async');

var MoneyControl = require('./moneyControl');
var CurrentPrice = require('./currentPrice');

var app = require('../server');
const CompanyAnnouncement = app.models.CompanyAnnouncement;

module.exports = function (data) {
  async.eachSeries(data, function (company, cb) {

    MoneyControl(company.BSECode, function (err, announcements) {
      if (err) {
        console.log('MC announcement err', err);
        return cb();
      }
      console.log('MC Announcement fetched', announcements.length);

      function done(err) {
        console.log('MC Announcement tracking done');
        return cb();
      }

      function check(info, callback) {
        CompanyAnnouncement.findOne({
          where: {
            title: info.heading,
            description: info.news,
            docLink: info.pdfLink,
            // announceTime: moment.utc(info.recievedTime).format()
          },
        }, function (err, result) {
          if (err || result) {
            // return done()
            return callback();
          }

          CurrentPrice(company.BSESymbol, function (err, price) {
            var information = {
              companyId: company.companyId,
              source: info.source,
              title: info.heading,
              description: info.news,
              docLink: info.pdfLink,
              announceTime: moment.utc(info.recievedTime).format(),
              createTime: moment.utc().format(),
            };

            if (price) {
              information.announcePrice = price.regularMarketPrice;
            } else {
              information.announcePrice = 0;
            }

            console.log('MC creating new announcement', information.title);
            CompanyAnnouncement.create(information, function (err, abc) {
              callback();
            });
          });
        });
      }

      async.eachSeries(announcements, check, done);

    });
  }, function (err) {
    if (err) {
      console.log('MC Tracking err', err);
    }
    console.log('MC Tracking done');
  });
};
