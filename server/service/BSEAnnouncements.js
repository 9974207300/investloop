var request = require('request');
var cheerio = require('cheerio');
var q = require('q');
var moment = require('moment');

module.exports = function GetBSEAnnouncements(code, cb){
  var xyz = [];
  var url = 'http://www.bseindia.com/corporates/ann.aspx?scrip=' + code;
  var news = [];
  var news_final = [];
  var index = [];
  request(url, function (err, response, body) {
    if(err) {
      return cb(true, []);
    }

    var i = body.indexOf('<tr><td class=\'announceheader\'');
    while (i != -1) {
      index.push(i);
      i = body.indexOf('<tr><td class=\'announceheader\'', (i + 1));
    }
  
    news = index.map((i, ind) => {
      var last;
      if (ind == (index.length - 1)) {
        last = body.indexOf('</table>', i);
      } else {
        last = index[ind + 1];
      }
      info = body.substring(i, last);
  
      var index1 = info.indexOf('TTRow_leftnotices');
      var index2 = info.indexOf('<tr><td class=\'TTHeadergrey\'', index1);
      var index3 = info.indexOf('<tr><td class=\'TTHeadergrey\'');
      var header = info.substring(0, index3);
  
      if (index2 != -1) {
        while (index2 != -1) {
          var str = '<table>' + header + info.substring(index3, index2) + '</table>';
          news_final.push(str);
  
          index1 = info.indexOf('TTRow_leftnotices', index2);
          index3 = index2;
          index2 = info.indexOf('<tr><td class=\'TTHeadergrey\'', index1);
  
          if (index2 == -1) {
            str = '<table>' + header + info.substr(index3) + '</table>';
            news_final.push(str);
            // return info
          }
        }
      } else {
        str = '<table>' + info + '</table>';
        news_final.push(str);
        // return info
      }
  
      return info;
    });
  
    var promise = news_final.map(function (info) {
  
      var defer = q.defer();
  
      var $ = cheerio.load(info);
  
      var dt = $('td[style="background-color:white"][colspan="4"]').text().trim();
      var dt_index = dt.indexOf('Exchange Disseminated Time') + 29;
      var dates = $('.announceheader').text().trim();
      var zone = '+05:30';
      var time;
      if(dt.length != 26){
        time = moment.utc(dt.substring(dt_index, dt_index + 21).trim()+zone, ['DD/MM/Y HH:mm:ssZ',]).format();
      } else {
        time = moment.utc(dates, ['DD MMM Y',]).format();        
      }
      var data = {
        date: $('.announceheader').text(),
        recievedTime: time,
        heading: $('.TTHeadergrey').first().text().replace('&#39;', '\'').replace('&amp;', '&'),
        news: $('.TTRow_leftnotices').first().text().replace('&#39;', '\'').replace('&amp;', '&'),
        source: 'BSE',
        pdfLink: $('a[target]')['0'] ? ($('a[target]')['0'].attribs ? $('a[target]')['0'].attribs.href : '') : '',
      };
  
      xyz.push(data);
  
      defer.resolve();
      return defer.promise;
    });
  
    q.all(promise)
      .then(function () {
        cb(null, xyz);
      })
      .catch(function(err){
        cb(err, []);
      });
  });
};