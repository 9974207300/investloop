const Pusher = require('pusher');

const pusher = new Pusher({
  appId: `${process.env.PUSHER_APP_ID}`,
  key: `${process.env.PUSHER_API_KEY}`,
  secret: `${process.env.PUSHER_API_SECRET}`,
  cluster: `${process.env.PUSHER_APP_CLUSTER}`,
  encrypted: true,
});

module.exports = function PusherFunction(channel, event, data) {
  pusher.trigger(channel, event, {
    'data': data,
  });
};