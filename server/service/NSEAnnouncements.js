var request = require('cloudscraper');
var cheerio = require('cheerio');
var q = require('q');
var moment = require('moment');

module.exports = function GetNSEAnnouncements(code, cb) {
  var url ='https://www.nseindia.com/marketinfo/companyTracker/corpAnnounce.jsp?symbol='+ code;
  var NSEdata = [];
  var NSEAnnouncements = [];
  request.get(url, function (error, response, body) {
    if (error){
      return cb(true, []);
    }
    
    var index = body.indexOf('<tr');

    while(index != -1){
      var endIndex = body.indexOf('</tr>', index);
      var str = '<table>'+body.substring(index, (endIndex+5))+'</table>';
      NSEdata.push(str);
      index = body.indexOf('<tr', endIndex);
    }

    var promise = NSEdata.map((data) => {
      var defer = q.defer();
      var $ = cheerio.load(data);
      var zone = '+05:30';
      var text = $('td').text();
      var link = $('a')['0'].attribs.onclick;
      link = 'https://www.nseindia.com' + link.substring(25, link.indexOf(',')-1);
      request.get(link, function(err, res, bd){
        if(err || !bd) {
          return defer.resolve();
        }
        var $1 = cheerio.load(bd);
        var description = $1('[class="contenttext"][align="justify"]').text();
        var xyz = {
          date: text.substring(text.length-20, text.length-8),
          recievedTime: moment.utc(text.substring(text.length-20).trim()+zone, ['MMM DD,Y,HH:mmZ',]).format(),
          heading:text.substring(3, text.length-20).trim().replace('&#39;', '\'').replace('&amp;', '&'),
          news: description.replace('&#39;', '\'').replace('&amp;', '&'),
          source: 'NSE',
          pdfLink: '',
        };
        NSEAnnouncements.push(xyz);
        defer.resolve();
      });

      return defer.promise;
    });

    q.all(promise)
      .then(function(){
        cb(null, NSEAnnouncements);
      });
  });
};
