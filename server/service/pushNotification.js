'use strict';

const async = require('async');
const path = require('path');
const webpush = require('web-push');

var app = require('../server');

const vapidKeys = {
  'publicKey': process.env.VAPID_PUBLIC_KEY,
  'privateKey': process.env.VAPID_PRIVATE_KEY,
};

webpush.setVapidDetails(
  'mailto:brijesh@jumpbyte.com',
  vapidKeys.publicKey,
  vapidKeys.privateKey
);

module.exports = function PushNotification(endpoints, body) {
  endpoints = Array.from(endpoints.reduce((m, t) => m.set(t.endpoint, t), new Map()).values());
  const notificationPayload = {
    'notification': {
      'title': body.title,
      'body': body.body,
      'icon': '/images/appgambit-logo.png',
      'vibrate': [100, 50, 100,],
      'actions': [],
    },
  };

  // if (action) {
  //   notificationPayload.action.push(action);
  // }

  Promise.all(endpoints.map(endpoint => {
    webpush.sendNotification(
      endpoint,
      JSON.stringify(notificationPayload)
    );
  }
  ))
    .then(() => console.log({ message: 'Newsletter sent successfully.', }))
    .catch(err => {
      console.error('Error sending notification, reason: ', err);

    });
};