'use-strict';

var yahooFinance = require('yahoo-finance');
var moment = require('moment');

module.exports = function GetCurrentPrice(symbol, cb) {
  try {
    if(symbol == '.NS' || symbol == '.BO'){
      cb(true, null);
    } else {
      yahooFinance.quote({
        symbol: symbol,
        modules: ['price',],
      }, function(err, quotes){
        if(err){
          return cb(true, null);
        }else{
          return cb(null, quotes.price);
        }
      });
    }
  } catch (err) {
    return cb(true, null);
  }
};