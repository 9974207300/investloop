module.exports = {
  'loopback-component-explorer': {
    'mountPath': process.env.EXPLORER,
    'generateOperationScopedModels': true,
  },
};
