'use strict';
var ObjectId = require('mongodb').ObjectId;

module.exports = function (app) {
  const Role = app.models.Role;
  const Users = app.models.Users;
  const RoleMapping = app.models.RoleMapping;
  RoleMapping.defineProperty('principalId', { type: ObjectId, });

  Users.afterRemote('login', function (ctx, instance, next) {
    const userId = ctx.result.userId;

    RoleMapping.find({
      where: {
        principalId: userId,
      },
      include: 'role',
    }, (err, roles) => {
      ctx.result.role = roles[0].role().name;
      next();
    });

  });

  Users.userRole = function(id, callback) {
    RoleMapping.find({
      where: {
        principalId: id,
      },
      include: 'role',
    }, function(err, roles){
      if(err) {
        callback(null, 'users');
      } else {
        callback(null, roles[0].role().name);
      }
    });
  };

  Users.remoteMethod('userRole', {
    http: {
      path: '/:id/role',
      verb: 'get',
    },
    returns: {
      arg: 'role',
      type: 'string',
    }, 
    accepts: {
      arg: 'id',
      type: 'string',
    },
  });
};
