'use-strict';

var ontime = require('ontime');
var moment = require('moment');
var _ = require('lodash');
const path = require('path');
const fs = require('fs');
const ejs = require('ejs');
var async = require('async');
var pushNotification = require('../service/pushNotification');

module.exports = function(server) {
  ontime({
    cycle: '12:00:00',
  }, function(ot) {
    const day =  moment().utcOffset('+0530').day();
    const weekend = [0, 6,].indexOf(day) !== -1;
    console.log('sending subscriber emails', moment().format());

    const Subscribers = server.models.Subscribers;
    const UserCompanies = server.models.UserCompanies;
    const Email = server.models.Email;

    UserCompanies.find({
      where: {
        public: true,
      },
      include: 'companies',
    }, function(err, company) {
      if (err) {
        console.log('Email sending to subscriber failed 1', err);
      } else {
        if (company && company.length > 0) {
          const length = company.length;
          const userCompanys = company.map(data => {
            data = data.toJSON();
            data.change = (data.companies.price - data.companies.previousPrice) * 100 / data.companies.previousPrice;
            data.change = +data.change.toFixed(2);
            data.recommendedChange = (data.companies.price - data.recommendedPrice) * 100 / data.recommendedPrice;
            data.recommendedPrice = +data.recommendedPrice.toFixed(2);
            data.recommendedSign = +data.recommendedChange.toFixed(2);
            data.recommendedChange = Math.abs(+data.recommendedChange.toFixed(2));
            return data;
          })
            .sort((a, b) => {
              return b.change - a.change;
            });

          const positiveMove = userCompanys.filter((data, index) => {
            if (index < 2) {
              return data.change > 0;
            } else {
              return false;
            }
          });

          userCompanys.reverse();
          const negativeMove = userCompanys.filter((data, index) => {
            if (index < 2) {
              return data.change < 0;
            } else {
              return false;
            }
          })
          .map(data => {
            data.change = data.change*-1;
            return data;
          });

          const dt = moment().utc().subtract(1, 'd').seconds(0).format();
          const newEntries = userCompanys.filter(data => {
            const rt = moment(data.createdOn).isAfter(moment(dt));
            return rt;
          });

          if (newEntries.length === 0 && weekend) return;

          Subscribers.find({
            where: {
              verified: true,
            },
          }, function(err, subscribers) {
            if (err) {
              console.log('Email sending to subscriber failed 2', err);
            } else if (subscribers && subscribers.length > 0) {
              const endpoints = [];

              subscribers.map(data => data.pushMessageData.map(endpoint => endpoints.push(endpoint)));
              const notification = {
                'title': `Stock movement - ${moment().format('DD MMM')}`,
                'body': 'Check your inbox for latest updates on stock movement',
              };

              pushNotification(endpoints, notification);

              const url = `http://${process.env.APP_URL}:${process.env.APP_PORT}/announcements`;
              const newsLetterTemplatePath = path.resolve(__dirname, '../app/views/newsLetter.ejs');
              const compiled = ejs.compile(fs.readFileSync(newsLetterTemplatePath, 'utf8'));

              async.eachSeries(subscribers, function(data, cb) {
                const text = `http://${process.env.APP_URL}:${process.env.APP_PORT}/api/subscribers/${data.id}/unsubscribe`;
                const html = compiled({weekend: weekend, text: text, url: url, company: newEntries, positiveMove: positiveMove, negativeMove: negativeMove,});
                const options = {
                  to: data.email,
                  from: 'InvestLoop <no-reply@appgambit.com>',
                  subject: `Stock Movements for ${moment().utcOffset('+0530').format('DD MMM')}`,
                  html: html,
                };

                Email.send(options, function(err, mail) {
                  if (err) {
                    console.log('Daily news letter sending err', err);
                  }
                });
                cb();
              }, function() {
                console.log('Subscriber email sent.');
              });
            }
          });
        }
      }
    });

    ot.done();
    return;
  });
};
