'use strict';
var moment = require('moment');
var BSECheck = require('../service/BSEAnnouncementCheck');
var NSECheck = require('../service/NSEAnnouncementCheck');
var ActionCheck = require('../service/corpActionCheck');
var NewsCheck = require('../service/companynewsCheck');
var MCCheck = require('../service/moneyControlCheck.js');

module.exports = function(app) {
  const TrackableCompanies = app.models.TrackableCompanies;

  setInterval(() => {
    var now = moment.utc().format();
    console.log('Fetching annoucements', now);

    TrackableCompanies.find({
      where: {
        nextTrackTime: {
          lte: now,
        },
        userTracking: {
          ne: 0,
        },
      },
    }, function(err, companies) {
      console.log('companies fetched', companies);
      if (!err && companies && companies.length) {
        companies.forEach((company) => {
          company.updateAttributes({
            lastTrackTime: moment.utc().format(),
            nextTrackTime: moment.utc().add(company.trackingInterval, 'minutes').format(),
          }, function() {
            console.log('company updated', company.id);
          });
        });

        BSECheck(companies);
        NSECheck(companies);
        ActionCheck(companies);
        // NewsCheck(companies);
        MCCheck(companies);
      }
    });
  }, 1 * 60 * 1000);
};
