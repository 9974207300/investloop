// 'use-strict';

// var aws = require('aws-sdk');
// var _ = require('lodash');
// var jsonexport = require('jsonexport');
// var ontime = require('ontime');
// var moment = require('moment');

// aws.config.update({
//   accessKeyId: process.env.AWS_ACCESS_KEY,
//   secretAccessKey: process.env.AWS_SECRET_KEY,
//   region: process.env.AWS_REGION
// });

// var s3 = new aws.S3({ params: { Bucket: 'investloop-announcement-bucket' } });

// module.exports = function (server) {

//   ontime({
//     cycle: "12:30:00"
//   }, function (ot) {
//     console.log("updating s3 bucket with new data", moment().format());
//     const AnnouncementComprehend = server.models.AnnouncementComprehend;
//     AnnouncementComprehend.find({

//     }, function (err, data) {
//       const mLData = _.map(data, (obj) => _.pick(obj, ['title_key_phrase', 'description_key_phrase', 'title_entity', 'description_entity', 'docLink_source', 'keep']));
//       jsonexport(mLData, function (err, csv) {
//         if (csv) {
//           const params = {
//             Key: process.env.ML_Announcement_File,
//             Body: csv
//           };

//           s3.upload(params, function (err, obj) {
//             console.log("s3 bucket updated", moment().format())
//           });
//         }
//       });
//     });
//     ot.done()
//     return
//   });
// }