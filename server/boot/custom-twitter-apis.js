'use strict';
var Twitter = require('twitter');

var client = new Twitter({
  'consumer_key': process.env.TWITTER_CONSUMER_KEY,
  'consumer_secret': process.env.TWITTER_CONSUMER_SECRET,
  'access_token_key': process.env.TWITTER_ACCESS_TOKEN_KEY,
  'access_token_secret': process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

module.exports = function(server) {
  var router = server.loopback.Router();

  router.get('/api/custom/twitter/friends', function(req, res) {
    client.get('friends/list', {}, function(err, user, response) {
      if (err) {
        res.json({
          success: false,
          error: err,
        });
      } else {
        res.json({
          success: true,
          friends: user,
        });
      }
    });
  });

  router.get('/api/custom/twitter/friends/pending', function(req, res) {
    client.get('friendships/outgoing', {}, function(err, user, response) {
      if (err) {
        res.json({
          success: false,
          error: err,
        });
      } else {
        res.json({
          success: true,
          pending: user,
        });
      }
    });
  });

  router.get('/api/custom/twitter/search-users', function(req, res) {
    const params = {
      count: 12,
      q: req.query.name,
    };

    client.get('users/search', params, function(err, user, response) {
      if (err) {
        res.json({
          success: false,
          error: err,
        });
      } else {
        res.json({
          success: true,
          users: user,
        });
      }
    });
  });

  router.post('/api/custom/twitter/add-friend', function(req, res) {
    const params = {
      'user_id': req.query.id,
    };

    client.post('friendships/create', params, function(err, user, response) {
      if (err) {
        res.json({
          success: false,
          error: err,
        });
      } else {
        res.json({
          success: true,
          users: user,
        });
      }
    });
  });

  router.post('/api/custom/twitter/remove-friend', function(req, res) {
    const params = {
      'user_id': req.query.id,
    };

    client.post('friendships/destroy', params, function(err, user, response) {
      if (err) {
        res.json({
          success: false,
          error: err,
        });
      } else {
        res.json({
          success: true,
          users: user,
        });
      }
    });
  });

  server.use(router);
};
