'use strict';

var async = require('async');
var moment = require('moment');
var CurrentPrice = require('../service/currentPrice');
var checkAlerts = require('../service/checkAlerts');

const pusher = require('../service/pusherService');

module.exports = function(app) {
  const TrackableCompanies = app.models.TrackableCompanies;
  const Companies = app.models.Companies;

  setInterval(() => {
    const now = moment().utcOffset('+0530').hours();
    if (now > 8 && now < 17) {
      TrackableCompanies.find({
        where: {
          userTracking: {
            ne: 0,
          },
        },
      }, function(err, data) {
        async.eachSeries(data, function(company, cb) {
          CurrentPrice(company.BSESymbol, function(err, price) {
            if (price) {
              Companies.updateAll({
                id: company.companyId,
              },
              {
                price: price.regularMarketPrice.raw || price.regularMarketPrice,
                previousPrice: price.regularMarketPreviousClose.raw || price.regularMarketPreviousClose,
              }, function(err, data) {
                if (err) {
                  console.log('price error', err);
                } else if (company.activeAlert && company.activeAlert > 0) {
                  checkAlerts(company,  price.regularMarketPrice.raw || price.regularMarketPrice);
                }
                cb();
              });
            } else {
              cb();
            }
          });
        }, function() {
          console.log('*****************Company Price Updated******************');
          pusher('Price', 'price-update', null);
        });
      });
    }
  }, 10 * 60 * 1000);
};
