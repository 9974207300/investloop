'use strict';

module.exports = function (server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  // router.get('/', server.loopback.status());

  // router
  //   .get('/api/*', function (req, res, next) {
  //     const AccessToken = server.models.AccessToken;

  //     if (req.query.public === 'true' || (req.query.uid && req.query.token)) {
  //       return next()
  //     } else {
  //       AccessToken.resolve(req.query.access_token, function (err, token) {
  //         if (token) {
  //           return next()
  //         } else {
  //           var error = new Error();
  //           error.status = 401;
  //           error.message = "AccessToken Invalid";
  //           return next(error);
  //         }
  //       });
  //     }
  //   });

  server.use(router);
};
