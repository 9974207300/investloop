'use strict';

const TAGS = require('../service/tags');

module.exports = function (server) {
  const Tags = server.models.Tags;

  Tags.find({}, function(err, data){
    TAGS.create(data);
    console.log('tag array initial value', TAGS.get());
  });

};
