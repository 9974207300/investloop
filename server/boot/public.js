'use strict';

module.exports = function(app) {
  const UserCompanies = app.models.UserCompanies;

  UserCompanies.public = function(cb) {
    UserCompanies.find({
      'where': {
        'public': true,
      },
      'include': {'relation': 'companies', 'scope': {'include': [{'relation': 'companyReports', 'scope': {'fields': [], 'where': {'public': true,},},}, {'relation': 'companyAnnouncements', 'scope': {'fields': [],},},],},},}, function(err, companies) {
      if (err) {
        cb(true, false);
      } else {
        cb(null, companies);
      }
    });
  };

  UserCompanies.remoteMethod(
    'public', {
      http: {
        path: '/public',
        verb: 'get',
      },
      returns: {
        arg: 'public',
        type: 'object',
      },
    }
  );
};
