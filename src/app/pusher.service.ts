import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

declare const Pusher: any;
declare const PUSHER_CLUSTER: any;
declare const PUSHER_KEY: any;

@Injectable()
export class PusherService {
  pusher: any;
  channel: any;
  constructor() {

    // const key = PUSHER_KEY.toString();
    // const cluster = PUSHER_CLUSTER.toString();

    this.pusher = new Pusher(PUSHER_KEY, { /// tslint:disable-line
      cluster: PUSHER_CLUSTER, // tslint:disable-line
      encrypted: true
    });

    this.channel = this.pusher.subscribe('Price');
  }

}
