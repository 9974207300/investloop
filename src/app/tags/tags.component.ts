import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort } from '@angular/material';
import { HttpClient, HttpParams } from '@angular/common/http';

import { MatChipInputEvent } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';

import { MenuComponent } from '../menu/menu.component';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

  tags: any;

  constructor(
    public dialog: MatDialog,
    private http: HttpClient
  ) { }

  ngOnInit() {
    const url = '/api/tags';

    this.http.get(url)
      .subscribe(res => this.tags = res);
  }

  AddTags() {
    const dialogRef = this.dialog.open(TagsFormComponent, {
      width: '450px',
      data: ''
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          let arr = result.tag.split(' ');
          result.tag = '';

          arr.forEach(element => {
            result.tag += element.charAt(0).toUpperCase() + element.slice(1) + ' ';
          });

          const url = '/api/tags';
          this.http.post(url, result)
            .subscribe(
            res => this.tags.push(res)
            );
        }
      });
  }

  RemoveTags(tag, index) {
    const url = `/api/tags/${tag.id}`;

    this.http.delete(url)
      .subscribe(
      res => this.tags.splice(index, 1)
      );
  }
}

/************************************************************* */

@Component({
  selector: 'app-tag-form',
  templateUrl: './tag-form.html',
  styleUrls: [],
  providers: []
})
export class TagsFormComponent implements OnInit {
  tagValidation = new FormControl('', [Validators.required]);
  fieldsValidation = new FormControl('', [Validators.required]);

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;

  form = {
    tag: '',
    fields: [],
  };

  separatorKeysCodes = [ENTER, COMMA];

  constructor(
    public dialogRef: MatDialogRef<TagsFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }


  ngOnInit() { }

  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // Add our fruit
    if ((value || '').trim() && !this.form.fields.includes(value.trim().toLowerCase())) {
      this.form.fields.push(value.trim().toLowerCase());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(tag: any): void {
    const index = this.form.fields.indexOf(tag);

    if (index >= 0) {
      this.form.fields.splice(index, 1);
    }
  }
}
