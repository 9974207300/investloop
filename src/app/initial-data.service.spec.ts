import { TestBed, inject } from '@angular/core/testing';

import { InitialDataResolver } from './initial-data.service';

describe('InitialDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InitialDataResolver]
    });
  });

  it('should be created', inject([InitialDataResolver], (service: InitialDataResolver) => {
    expect(service).toBeTruthy();
  }));
});
