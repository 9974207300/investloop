import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class AppServiceService {

  constructor(
    private http: HttpClient
  ) { }

  checkEmail(email) {
    const url = `/api/subscribers/${email}/exists`;
    return this.http.get(url);
  }

  subscribe(data) {
    const url = `/api/subscribers/`;
    return this.http.post(url, data);
  }

  pushMessage(email, data) {
    const url = `/api/subscribers/${email}/push`;
    return this.http.post(url, {data: data});
  }

}
