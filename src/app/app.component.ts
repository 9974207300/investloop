import { Component, HostListener } from '@angular/core';
import { AppServiceService } from './app-service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { SwPush } from '@angular/service-worker';
import Swal from 'sweetalert';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent {

  constructor(public dialog: MatDialog) { }

  // @HostListener('window:openModal', ['$event'])
  // openModal(event) {
  //    setTimeout(() => this.SubscriberModal(), 5000);
  // }

  SubscriberModal() {
    const dialogRef = this.dialog.open(SubscriberModalComponent, {
      width: '450px'
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result === 'Success') {
          Swal('Subscribed Successfully', 'Email Verification link has been sent', 'success')
          .then();
        } else if (result === 'Already') {
          Swal('You have already subscribed!')
          .then();
        }
      });
  }
}

/**************************************************************** */

@Component({
  selector: 'app-subscriber-modal',
  templateUrl: './subscriber-modal.html',
  styleUrls: [],
  providers: [AppServiceService]
})
export class SubscriberModalComponent {
  readonly VAPID_PUBLIC_KEY = 'BE05x62kahLs-t0euqt-w8QNrU_9mooYxwdkfw7YhcHWF0W2VoFWx8Zk3-iNA-Vi7oOxtcIbJGPxcnCNbf1TzHU';

  email: string;
  emailValidation = new FormControl('', [Validators.required, Validators.email]);

  constructor(
    private swPush: SwPush,
    private dialogRef: MatDialogRef<SubscriberModalComponent>,
    private appService: AppServiceService
  ) { }

  getEmailErrorMessage() {
    return this.emailValidation.hasError('required') ? 'Email is required' :
      this.emailValidation.hasError('email') ? 'Not a valid email' :
        '';
  }

  Subscribe() {
    const that = this;
    this.appService.checkEmail(this.email)
      .subscribe(res => {
        if (!res['exists']) {
          const subscriberData = {
            email: this.email
          };

          this.appService.subscribe(subscriberData)
            .subscribe(result => that.PushMessage());
          this.dialogRef.close('Success');
        } else {
          this.dialogRef.close('Already');
          that.PushMessage();
        }
      });
  }

  PushMessage() {
    this.swPush.requestSubscription({
      serverPublicKey: this.VAPID_PUBLIC_KEY
    })
      .then(push => {
        this.appService.pushMessage(this.email, push)
          .subscribe();
      }).catch(error => console.log(error));
  }
}

// if (Notification['permission'] === 'default') {
//   const event = new Event('openModal', { bubbles: true });
//   window.onload = () => window.dispatchEvent(event);
// }
