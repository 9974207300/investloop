import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-router.module';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

import { FlexLayoutModule } from '@angular/flex-layout';

import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';
import { httpInterceptorProviders } from './http-interceptor/index';

import { AppComponent, SubscriberModalComponent } from './app.component';
import { LoginComponent, /*VerificationEmailComponent*/ } from './login/login.component';
import { CompanyAnnouncementsComponent } from './company-announcements/company-announcements.component';
import { UserCompaniesComponent, UserCompanyNotificationComponent, AlertComponent } from './user-companies/user-companies.component';
import { AdminCompaniesComponent } from './admin-companies/admin-companies.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { PortfolioFormComponent } from './portfolio/portfolio.component';
import { MenuComponent, ChangePasswordComponent } from './menu/menu.component';
import {
  PublicComponent,
  ChartViewComponent,
  CompanyFormComponent,
  CompanyReportFormComponent,
  GetCompanyReportComponent,
  EditUserCompanyComponent
} from './public-companies/public.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { AnnouncementVerificationComponent } from './announcement-verification/announcement-verification.component';
import { TagsComponent, TagsFormComponent } from './tags/tags.component';
import { CompanyViewComponent } from './company-view/company-view.component';

import { ShareButtonsOptions } from '@ngx-share/core';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { TwitterComponent } from './twitter/twitter.component';
import { SeoService } from './seo.service';

const customOptions: ShareButtonsOptions = {
  include: ['whatsapp', 'email', 'copy'],
  exclude: [],
  theme: 'modern-light'
};

@NgModule({
  declarations: [
    AppComponent,
    UserCompaniesComponent,
    CompanyAnnouncementsComponent,
    LoginComponent,
    AdminCompaniesComponent,
    PublicComponent,
    PortfolioComponent,
    PortfolioFormComponent,
    MenuComponent,
    CompanyFormComponent,
    ConfirmationDialogComponent,
    ChangePasswordComponent,
    CompanyReportFormComponent,
    GetCompanyReportComponent,
    EditUserCompanyComponent,
    ResetPasswordComponent,
    UserCompanyNotificationComponent,
    AnnouncementVerificationComponent,
    TagsComponent,
    TagsFormComponent,
    AlertComponent,
    SubscriberModalComponent,
    CompanyViewComponent,
    ChartViewComponent,
    TwitterComponent
    // VerificationEmailComponent
  ],
  entryComponents: [
    AdminCompaniesComponent,
    PortfolioFormComponent,
    CompanyFormComponent,
    ConfirmationDialogComponent,
    ChangePasswordComponent,
    CompanyReportFormComponent,
    GetCompanyReportComponent,
    EditUserCompanyComponent,
    UserCompanyNotificationComponent,
    TagsFormComponent,
    AlertComponent,
    SubscriberModalComponent,
    ChartViewComponent
    // VerificationEmailComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    FlexLayoutModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatCardModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    MatChipsModule,
    MatDialogModule,
    MatCheckboxModule,
    MatTabsModule,
    MatTableModule,
    MatAutocompleteModule,
    MatSortModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatRadioModule,
    MatMomentDateModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    ShareButtonsModule.forRoot({ options: customOptions })
  ],
  providers: [
    AuthGuardService,
    AuthService,
    httpInterceptorProviders,
    SeoService,
    // InitialDataResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
