import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MenuComponent } from '../menu/menu.component';
import { AuthService } from '../auth.service';
import { TwitterService } from './twitter.service';

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.css'],
  providers: [TwitterService]
})
export class TwitterComponent implements OnInit {

  friends = [];
  pendingReq = [];
  user_name = '';
  searchUser = [];

  loader = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private twitter: TwitterService
  ) {
    this.authService.getRole(role => {
      if (!role) {
        this.router.navigate(['']);
      }
    });
  }

  ngOnInit() {
    this.loader = true;
    this.twitter.getFriends()
      .subscribe(
        (friends: any) => {
          this.loader = false;
          if (friends.success) {
            this.friends = friends.friends.users;
          }
        }
      );

      // this.twitter.pendingRequest()
      // .subscribe(
      //   (friends: any) => {
      //     this.loader = false;
      //     if (friends.success) {
      //       this.pendingReq = friends.pending.users;
      //     }
      //   }
      // );
  }

  Search() {
    this.loader = true;
    this.twitter.searchUsers(this.user_name)
    .subscribe(
      (users: any) => {
        this.loader = false;
        if (users.success) {
          this.searchUser = users.users;
        }
      }
    );
  }

  AddFriend(data, index) {
    this.loader = true;
    this.twitter.addFriend(data.id_str)
    .subscribe(
      (user: any) => {
        this.loader = false;
        if (user.success) {
          this.friends.push(data);
          this.searchUser.splice(index, 1);
        }
      }
    );
  }

  RemoveFriend(data, index) {
    this.loader = true;
    this.twitter.removeFriend(data.id_str)
    .subscribe(
      (user: any) => {
        this.loader = false;
        if (user.success) {
          this.friends.splice(index, 1);
        }
      }
    );
  }

}
