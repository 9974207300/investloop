import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class TwitterService {

  constructor(
    private http: HttpClient
  ) { }

  getFriends() {
    const url = `/api/custom/twitter/friends`;
    return this.http.get(url);
  }

  searchUsers(name) {
    const url = `/api/custom/twitter/search-users`;
    return this.http.get(url, {
      params: new HttpParams()
      .set('name', name)
    });
  }

  pendingRequest() {
    const url = `/api/custom/twitter/friends/pending`;
    return this.http.get(url);
  }

  addFriend(id) {
    const url = `/api/custom/twitter/add-friend`;
    return this.http.post(url, {}, {
      params: new HttpParams()
      .set('id', id)
    });
  }

  removeFriend(id) {
    const url = `/api/custom/twitter/remove-friend`;
    return this.http.post(url, {}, {
      params: new HttpParams()
      .set('id', id)
    });
  }

}
