import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyAnnouncementsService } from '../company-announcements/company-announcements.service';
import { MenuComponent } from '../menu/menu.component';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-announcement-verification',
  templateUrl: './announcement-verification.component.html',
  styleUrls: ['./announcement-verification.component.css'],
  providers: [CompanyAnnouncementsService, MatPaginator]
})
export class AnnouncementVerificationComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  announcements: any;
  selectedGroup = false;

  pageLength: number;
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 50, 100];

  constructor(
    private announcementService: CompanyAnnouncementsService
  ) { }

  ngOnInit() {
    const data = {
      verified: false,
      skip: 0,
      limit: 10
    };

    this.getAnnouncementCount(data);
    this.getAnnouncements(data);
  }

  getAnnouncements(data) {
    this.announcementService.getAllVerifiedAnnouncements(data)
      .subscribe(
      res => this.announcements = res
      );
  }

  getAnnouncementCount(data) {
    this.announcementService.getAllVerifiedAnnouncementsCount(data)
      .subscribe(
      res => this.pageLength = res['count']
      );
  }

  ChangeGroup(event) {
    this.paginator.pageIndex = 0;

    const value = JSON.parse(event.value);
    this.selectedGroup = value;

    const data = {
      verified: value,
      skip: 0,
      limit: 10
    };

    this.getAnnouncementCount(data);
    this.getAnnouncements(data);
  }

  ChangeStatus(data, event) {
    const option = {
      id: data.announcementComprehends.id,
      keep: event.value
    };
    this.announcementService.markAnnouncementVerified(option)
      .subscribe();
  }

  pageChange(event) {
    const data = {
      verified: this.selectedGroup,
      skip: event.pageSize * event.pageIndex,
      limit: event.pageSize
    };

    this.getAnnouncements(data);
  }
}
