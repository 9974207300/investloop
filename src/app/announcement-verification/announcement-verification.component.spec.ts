import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementVerificationComponent } from './announcement-verification.component';

describe('AnnouncementVerificationComponent', () => {
  let component: AnnouncementVerificationComponent;
  let fixture: ComponentFixture<AnnouncementVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnouncementVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
