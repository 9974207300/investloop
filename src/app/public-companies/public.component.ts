import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { PublicService } from './public.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { AdminCompaniesComponent } from '../admin-companies/admin-companies.component';
import { AuthService } from '../auth.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import * as moment from 'moment';
import Swal from 'sweetalert';

import { MenuComponent } from '../menu/menu.component';
import { PusherService } from '../pusher.service';
import { ShareButtons } from '@ngx-share/core';
import { SeoService } from '../seo.service';

const color = ['#007700', '#E74C3C'];
// let roleLoaded = false;
// let userRole = false;
let lastVisited = {};
const publicPageDetails = {
  lastVisited: '',
  announcements: {},
  reports: {}
};

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css'],
  providers: [PublicService, PusherService, SeoService]
})

export class PublicComponent implements OnInit {
  constructor(
    private userCompany: PublicService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private auth: AuthService,
    private activeRoute: ActivatedRoute,
    public share: ShareButtons,
    private pusherService: PusherService,
    public seo: SeoService
  ) {
    lastVisited = JSON.parse(localStorage.getItem('PublicPageDetails')) || publicPageDetails;
  }

  role: boolean;
  user_companies: any;
  user_cmpny: any;
  loader = false;
  tokenExp = false;
  displayedColumns = ['recommendedOn', 'name', 'recommendedPrice', 'price', 'action'];
  dataSource = new MatTableDataSource();

  sortData(sort: Sort) {
    let data = this.user_companies.slice();
    if (!sort.active || sort.direction === '') {
    } else {
      data = data.sort((a, b) => {
        const isAsc = sort.direction !== 'asc';
        switch (sort.active) {
          case 'name': return compare(a.companies.name, b.companies.name, isAsc);
          case 'price': return compare(+a.companies.price, +b.companies.price, isAsc);
          case 'change': return compare(+a.priceChange, +b.priceChange, isAsc);
          case 'recommendedOn': return compare(a.createdOn, b.createdOn, isAsc);
          case 'recommendedPrice': return compare(a.recommendedPrice, b.recommendedPrice, isAsc);
          default: return 0;
        }
      });
    }
    this.dataSource = new MatTableDataSource(data);
  }

  getUserCompany() {
    this.userCompany.getUserCompanies()
      .subscribe(company => {
        company = company['public'];
        this.user_cmpny = company;
        this.user_companies = this.user_cmpny.reverse().map((cmpny) => {

          if (!lastVisited || !lastVisited['announcements'][cmpny.companies.id]) {
            lastVisited['announcements'][cmpny.companies.id] = cmpny.companies.companyAnnouncements.length;
          }

          if (!lastVisited['reports'][cmpny.companies.id]) {
            lastVisited['reports'][cmpny.companies.id] = cmpny.companies.companyReports.length;
          }

          if (lastVisited) {
            cmpny.isNewReports = cmpny.companies.companyReports.length - lastVisited['reports'][cmpny.companies.id] || 0;
            cmpny.isNewAnnouncements = cmpny.companies.companyAnnouncements.length - lastVisited['announcements'][cmpny.companies.id] || 0;
            cmpny.isNew = moment(cmpny.createdOn).diff(moment(lastVisited['lastVisited'])) > 0 ? true : false;
          }
          cmpny.editDate = false;
          cmpny.priceChange = (cmpny.companies.price - cmpny.recommendedPrice) / cmpny.recommendedPrice;

          cmpny.description = `Recommended On: ${moment(cmpny.createdOn).format('DD MMM Y')},\nCompany: ${cmpny.companies.name},\nRecommended Price: Rs.${cmpny.recommendedPrice},\nCMP: Rs.${cmpny.companies.price} (${+(cmpny.priceChange * 100).toFixed(2)} %) \n`; // tslint:disable-line
          cmpny.social = false;

          if (cmpny.priceChange < 0) {
            cmpny.color = color[1];
            cmpny.priceChange = cmpny.priceChange * -1;
          } else {
            cmpny.color = color[0];
          }


          return cmpny;
        });

        lastVisited['lastVisited'] = moment.utc().format();
        localStorage.setItem('PublicPageDetails', JSON.stringify(lastVisited));
        this.dataSource = new MatTableDataSource(this.user_companies);
        this.loader = false;
      });
  }

  ngOnInit() {

    this.activeRoute.queryParams.subscribe((params: Params) => {
      const token = params['expired'];
      const subscribed = params['subscribed'];
      if (token || subscribed) {
        this.userCompany.getJWTEmail(token || subscribed)
          .subscribe(
            res => {
              if (res['email']) {
                if (token) {
                  Swal('Your link has expired!', `Another verification LInk has been sent to ${res['email']}`, 'warning')
                  .then();
                } else {
                  Swal('You have subscribed successfully', `Daily updates will be sent to ${res['email']}`, 'success')
                  .then();
                }
              }
            }
          );
      }
    });
    this.auth.getRole((roles) => {
      this.role = roles;
    });
    this.getUserCompany();

    this.pusherService.channel.bind('price-update', data => this.getUserCompany());
  }

  getCompanyAnnouncements = (company) => {
    // console.log(company, publicPageDetails, lastVisited);
    lastVisited['announcements'][company.companyId] = company.companies.companyAnnouncements.length;
    localStorage.setItem('PublicPageDetails', JSON.stringify(lastVisited));
    const company_url = `/announcements/${company.companyId}`;
    this.router.navigateByUrl(company_url);
  }

  removeUserCompany = (company) => {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: { dialog: 'delete' },
      disableClose: true,
      hasBackdrop: true
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.loader = true;
          const name = company.companies.name;
          this.userCompany.removeUserCompany(company.id)
            .subscribe(() => {
              this.getUserCompany();
              this.snackBar.open('Company removed successfully', 'close', {
                duration: 2000
              });
            });
        }
      });
  }

  addUserCompanyDialog = (): void => {
    const dialogRef = this.dialog.open(AdminCompaniesComponent, {
      width: '450px',
      height: '600px',
      data: this.user_companies
    });
    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.loader = true;
          this.userCompany.addUserCompany(result)
            .subscribe(() => {
              // lastVisited = publicPageDetails;
              this.getUserCompany();
              this.snackBar.open('Company added successfully', 'close', {
                duration: 2000
              });
            });
        }
      });
  }

  addCompanyDialog = (): void => {
    const dialogRef = this.dialog.open(CompanyFormComponent, {
      width: '600px'
    });

    dialogRef.afterClosed()
      .subscribe(
        result => {
          if (result) {
            this.loader = true;
            this.userCompany.addNewCompany(result)
              .subscribe(
                data => {
                  this.loader = false;
                  this.snackBar.open('Company added successfully', 'close', {
                    duration: 2000
                  });
                }
              );
          }
        }
      );
  }

  editUserCompany(element, index) {
    const data = {
      company: element.companies.name,
      createdOn: element.createdOn,
      recommendedPrice: element.recommendedPrice
    };

    const dialogRef = this.dialog.open(EditUserCompanyComponent, {
      width: '600px',
      data: data
    });

    dialogRef.afterClosed()
      .subscribe(
        result => {
          if (result) {
            this.loader = true;
            delete result.company;
            result.id = element.id;
            this.userCompany.updateUserCompany(result)
              .subscribe(
                res => {
                  this.loader = false;
                  this.updateUserCompany(res, index);
                  this.snackBar.open('Company updated successfully', 'close', {
                    duration: 2000
                  });
                });
          }
        }
      );
  }

  updateUserCompany(result, index) {
    this.user_companies[index].createdOn = result['createdOn'];
    this.user_companies[index].recommendedPrice = result['recommendedPrice'];
    this.user_companies[index].priceChange = this.user_companies[index].companies.price - result['recommendedPrice'];
    this.user_companies[index].priceChange = this.user_companies[index].priceChange / result['recommendedPrice'];
    if (this.user_companies[index].priceChange < 0) {
      this.user_companies[index].color = color[1];
      this.user_companies[index].priceChange = this.user_companies[index].priceChange * -1;
    } else {
      this.user_companies[index].color = color[0];
    }

    this.snackBar.open('Company recommended date updated successfully', 'close', {
      duration: 2000
    });
  }

  addCompanyReports(element, index) {
    const dialogRef = this.dialog.open(CompanyReportFormComponent, {
      width: '600px'
    });

    dialogRef.afterClosed()
      .subscribe(
        result => {
          if (result) {
            lastVisited['reports'][element.companyId] = lastVisited['reports'][element.companyId] + 1;
            localStorage.setItem('PublicPageDetails', JSON.stringify(lastVisited));
            this.loader = true;
            this.userCompany.addCompanyReport(element.companyId, result)
              .subscribe(
                data => {
                  this.loader = false;
                  this.user_companies[index].companies.companyReports.push({ 'companyId': element.companyId });
                  this.snackBar.open('Updates added successfully', 'close', {
                    duration: 2000
                  });
                });
          }
        });
  }

  getCompanyUpdate(element, index) {
    lastVisited['reports'][element.companyId] = element.companies.companyReports.length;
    localStorage.setItem('PublicPageDetails', JSON.stringify(lastVisited));
    this.user_companies[index].isNewReports = 0;
    const dialogRef = this.dialog.open(GetCompanyReportComponent, {
      width: '600px',
      data: element.companyId
    });

  }

  GraphView(company) {
    let data = [
      [
        company.name,
        `BSE:${company.NSECode}`
      ]
    ];

    data = [data[0], ...this.user_companies.map(cmpny => {
      if (cmpny.companies.id === company.id) {
        return null;
      }

      return [
        cmpny.companies.name,
        `BSE:${cmpny.companies.NSECode}`
      ];
    })];

    data = data.filter(cmpny => cmpny);

    const dialog = this.dialog.open(ChartViewComponent, {
      width: '80%',
      data: data
    });
  }

  SetMetaTag(element) {
    this.seo.updateTags({
      title: `${element.companies.name}-InvestLoop`,
      description: `CMP ₹${element.companies.price}`
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

/***************************************************************** */

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.html',
  providers: []
})
export class CompanyFormComponent implements OnInit {
  nameValidation = new FormControl('', [Validators.required]);
  BSEValidation = new FormControl('', [Validators.required]);
  NSEValidation = new FormControl('', [Validators.required]);

  form = {
    name: '',
    BSECode: '',
    NSECode: '',
    siteURL: ''
  };

  constructor(
    public dialogRef: MatDialogRef<CompanyFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }


  ngOnInit() { }

}

/***************************************************************** */

@Component({
  selector: 'app-company-report-form',
  templateUrl: './company-report-form.html',
  providers: []
})
export class CompanyReportFormComponent implements OnInit {
  titleValidation = new FormControl('', [Validators.required]);

  form = {
    title: '',
    description: '',
    source: '',
    public: true
  };

  constructor(
    public dialogRef: MatDialogRef<CompanyReportFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }


  ngOnInit() { }

}


/***************************************************************** */

@Component({
  selector: 'app-get-company-report',
  templateUrl: './company-report.html',
  styleUrls: ['./public.component.css'],
  providers: [PublicService]
})
export class GetCompanyReportComponent implements OnInit {

  report: any;
  companyId = '';

  pageLength: number;
  pageSize = 10;
  pageSizeOptions = [3, 5];

  constructor(
    public dialogRef: MatDialogRef<GetCompanyReportComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: PublicService
  ) {
    this.companyId = data;
  }


  ngOnInit() {
    const data = {
      companyId: this.companyId,
      skip: 0,
      limit: 10
    };

    this.service.getCompanyReportCount(data)
      .subscribe(result => {
        this.pageLength = result['count'];
      });

    this.getUpdates(data);
  }

  getUpdates(data) {
    this.service.getCompanyReport(data)
      .subscribe(result => {
        this.report = result;
        this.report.map(res => {
          res.createdOn = moment(res.createdOn).from(moment());
        });
      });
  }

  pageChange(event) {
    const data = {
      companyId: this.companyId,
      skip: event.pageIndex * event.pageSize,
      limit: event.pageSize
    };

    this.getUpdates(data);
  }

}

/***************************************************************** */

@Component({
  selector: 'app-user-company-form',
  templateUrl: '../user-company-form.html',
  styleUrls: ['./public.component.css'],
  providers: []
})
export class EditUserCompanyComponent implements OnInit {
  dateValidation = new FormControl('', [Validators.required]);
  priceValidation = new FormControl('', [Validators.required]);

  form = {
    company: '',
    createdOn: '',
    recommendedPrice: ''
  };

  editLastDate = moment().format();

  constructor(
    public dialogRef: MatDialogRef<EditUserCompanyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = data;
  }


  ngOnInit() { }

}

/***************************************************************** */

declare const TradingView: any;

@Component({
  selector: 'app-public-chart-view',
  templateUrl: './chart-view.html',
  styleUrls: [],
  providers: []
})
export class ChartViewComponent implements AfterViewInit {
  widget_data = {
    'container_id': 'tv-medium-widget',
    'symbols': [],
    // 'greyText': 'Quotes by',
    'gridLineColor': '#e9e9ea',
    'fontColor': '#83888D',
    'underLineColor': '#dbeffb',
    'trendLineColor': '#4bafe9',
    'width': '100%',
    'height': '400',
    'locale': 'in'
  };

  constructor(
    public dialogRef: MatDialogRef<ChartViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.widget_data.symbols = data;
  }


  ngAfterViewInit() {
    new TradingView.MediumWidget(this.widget_data); // tslint:disable-line
  }

}

