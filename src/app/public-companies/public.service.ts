import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class PublicService {

  accessToken = localStorage.getItem('accessToken');
  userId = localStorage.getItem('userId');

  constructor(private http: HttpClient) { }

  getUserCompanies() {
    const url = `/api/userCompanies/public`;
    return this.http.get(url);
  }

  updateUserCompany(element) {
    const url = `/api/userCompanies/${element.id}/`;
    delete element.id;
    return this.http.patch(url, element);
  }

  removeUserCompany(id) {
    const url = `/api/userCompanies/${id}`;
    return this.http.delete(url);
  }

  addUserCompany(id) {
    const url = `/api/users/${this.userId}/userCompanies`;
    return this.http.post(url, {
      companyId: id,
      public: true
    });
  }

  addNewCompany(data) {
    const url = `/api/companies/`;
    return this.http.post(url, data);
  }

  addCompanyReport(companyId, data) {
    const url = `/api/companies/${companyId}/companyReports`;
    return this.http.post(url, data);
  }

  getCompanyReport(data) {
    const url = `/api/companies/${data.companyId}/companyReports`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('filter', `{"where": {"public": true}, "order": "createdOn desc", "skip": ${data.skip}, "limit": ${data.limit}}`)
    });
  }

  getCompanyReportCount(data) {
    const url = `/api/companies/${data.companyId}/companyReports/count`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('filter', `{"where": {"public": true}}`)
    });
  }

  getJWTEmail(token) {
    const url = `/api/subscribers/${token}/jwt`;
    return this.http.get(url);
  }
}
