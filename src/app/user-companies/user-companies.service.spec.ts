import { TestBed, inject } from '@angular/core/testing';

import { UserCompaniesService } from './user-companies.service';

describe('UserCompaniesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserCompaniesService]
    });
  });

  it('should be created', inject([UserCompaniesService], (service: UserCompaniesService) => {
    expect(service).toBeTruthy();
  }));
});
