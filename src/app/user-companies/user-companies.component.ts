import { Component, OnInit, Inject } from '@angular/core';
import { UserCompaniesService } from './user-companies.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { MatSnackBar } from '@angular/material';
// import { SelectionModel } from '@angular/cdk/collections';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

import { AdminCompaniesComponent } from '../admin-companies/admin-companies.component';
import { AuthService } from '../auth.service';
import { MenuComponent } from '../menu/menu.component';

import { PusherService } from '../pusher.service';
import { Observable } from 'rxjs/Observable';

const color = ['#007700', '#E74C3C'];

let publicPageDetails = {
  lastVisited: '',
  announcements: {},
  reports: {}
};

let announcementDetails = {};

@Component({
  selector: 'app-user-companies',
  templateUrl: './user-companies.component.html',
  styleUrls: ['./user-companies.component.css'],
  providers: [UserCompaniesService, PusherService]
})

export class UserCompaniesComponent implements OnInit {
  constructor(
    private userCompany: UserCompaniesService,
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private auth: AuthService,
    private pusherService: PusherService
  ) {

    const userId = localStorage.getItem('userId');
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken || !userId) {
      this.router.navigate(['/login']);
    }

    publicPageDetails = JSON.parse(localStorage.getItem('PublicPageDetails')) || publicPageDetails;
    announcementDetails = publicPageDetails['announcements'];

  }

  user_companies: any;
  user_cmpny: any;
  userId: string;
  accessToken: string;
  portfolio: any;
  loader = false;

  displayedColumns = ['select', 'added', 'name', 'price', 'change', 'action'];
  dataSource = new MatTableDataSource();
  // selection = new SelectionModel(true, []);

  // isAllSelected() {
  //   const numSelected = this.selection.selected.length;
  //   const numRows = this.dataSource.data.length;
  //   return numSelected === numRows;
  // }

  // masterToggle() {
  //   this.isAllSelected() ?
  //     this.selection.clear() :
  //     this.dataSource.data.forEach(row => this.selection.select(row));
  //   this.user_companies.forEach((company) => {
  //     if (this.isAllSelected() !== company.notify) {
  //       company.notify = !company.notify;
  //       this.updateNotification(company);
  //     }
  //   });
  // }

  sortData(sort: Sort) {
    let data = this.user_companies.slice();
    if (!sort.active || sort.direction === '') {
    } else {
      data = data.sort((a, b) => {
        const isAsc = sort.direction !== 'asc';
        switch (sort.active) {
          case 'added': return compare(a.createdOn, b.createdOn, isAsc);
          case 'name': return compare(a.companies.name, b.companies.name, isAsc);
          case 'price': return compare(+a.companies.price, +b.companies.price, isAsc);
          case 'change': return compare(+a.priceChange, +b.priceChange, isAsc);
          default: return 0;
        }
      });
    }
    this.dataSource = new MatTableDataSource(data);
  }

  getUserCompany() {
    this.userCompany.getUserCompanies()
      .subscribe(company => {
        this.user_cmpny = company;
        this.user_companies = this.user_cmpny.map((cmpny) => {
          if (!announcementDetails[cmpny.companies.id]) {
            announcementDetails[cmpny.companies.id] = cmpny.companies.companyAnnouncements.length;
          }

          cmpny.isNewAnnouncements = cmpny.companies.companyAnnouncements.length - announcementDetails[cmpny.companies.id];

          cmpny.isNotify = cmpny.notify.length === 0 ? false : true;
          const alert = cmpny.alerts.filter(a => a.active);
          cmpny.isAlert = alert.length === 0 ? false : true;
          cmpny.difference = Math.abs(cmpny.companies.price - cmpny.companies.previousPrice);
          cmpny.priceChange = (cmpny.companies.price - cmpny.companies.previousPrice) / cmpny.companies.previousPrice;
          if (cmpny.priceChange < 0) {
            cmpny.color = color[1];
            cmpny.priceChange = cmpny.priceChange * -1;
          } else {
            cmpny.color = color[0];
          }

          if (this.portfolio.indexOf(cmpny.companyId) !== -1) {
            cmpny.disableRemove = true;
          }

          return cmpny;
        });
        this.dataSource = new MatTableDataSource(this.user_companies);
        this.loader = false;
        publicPageDetails['announcements'] = announcementDetails;
        localStorage.setItem('PublicPageDetails', JSON.stringify(publicPageDetails));
      });
  }

  ngOnInit(): void {
    this.userCompany.getPortfolioComapnies()
      .subscribe(
        data => {
          this.portfolio = data;
          this.portfolio = this.portfolio.map(profile => {
            return profile.companyId;
          });
          this.getUserCompany();
        });

    this.pusherService.channel.bind('price-update', data => this.getUserCompany());
  }

  OpenNotification(element, index) {
    const oldDataLength = element.notify.length;
    const oldNotification = element.notify;
    const dialogRef = this.dialog.open(UserCompanyNotificationComponent, {
      width: '450px',
      data: oldNotification
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const notify = result[0] && (result.map((a) => element.notify.indexOf(a) === -1).indexOf(true) !== -1);
          if (notify || (oldDataLength !== result.length)) {
            this.userCompany.updateNotifications(element.id, result)
              .subscribe(
                results => {
                  this.user_companies[index].notify = result,
                    this.user_companies[index].isNotify = result[0] ? true : false;
                }
              );
          }
        }
      });
  }

  OpenAlerts(element, index) {
    const dialogRef = this.dialog.open(AlertComponent, {
      width: '450px',
      data: element,
      disableClose: true,
      hasBackdrop: true
    });

    dialogRef.afterClosed()
      .subscribe(
        result => {
          if (result) {
            this.user_companies[index].alerts = result;
            this.user_companies[index].isAlert = result.filter(a => a.active).length !== 0;
          }
        }
      );
  }

  getCompanyAnnouncements = (company) => {
    const company_url = `/announcements/${company.companyId}`;
    this.router.navigateByUrl(company_url);
  }

  removeUserCompany = (company) => {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: { dialog: 'delete' },
      disableClose: true,
      hasBackdrop: true
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.loader = true;
          const name = company.companies.name;
          this.userCompany.removeUserCompany(company.id)
            .subscribe(() => {
              this.getUserCompany();
              this.snackBar.open('Company removed successfully', 'close', {
                duration: 2000
              });
            });
        }
      });
  }

  addUserCompanyDialog = (): void => {
    const dialogRef = this.dialog.open(AdminCompaniesComponent, {
      width: '450px',
      height: '600px',
      data: this.user_companies
    });
    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.loader = true;
          this.userCompany.addUserCompany(result)
            .subscribe(() => {
              this.getUserCompany();
              this.snackBar.open('Company added successfully', 'close', {
                duration: 2000
              });
            });
        }
      });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

/***************************************************************** */

@Component({
  selector: 'app-user-company-notification',
  templateUrl: './user-company-notification.html',
  styleUrls: ['./user-companies.component.css'],
  providers: [UserCompaniesService]
})
export class UserCompanyNotificationComponent implements OnInit {
  notificationType: any;
  notification = [];
  isAllSelected: boolean;
  tags = [];

  constructor(
    private dialogRef: MatDialogRef<UserCompanyNotificationComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    public userCompanyService: UserCompaniesService
  ) { }


  ngOnInit() {
    this.notificationType = [
      { active: false, value: 'All' },
      { active: false, value: 'BSE' },
      // { active: false, value: 'GOOGLE FINANCE' },
      { active: false, value: 'NSE' },
      { active: false, value: 'MONEY CONTROL' }
    ];

    this.userCompanyService.getTags()
      .subscribe(
        (tags: Array<any>) => {
          tags.forEach(element => {
            this.notificationType.push({
              active: false, value: element.tag
            });

            this.tags.push({
              active: false, value: element.tag
            });
          });

          this.notification = Object.assign([], this.data);

          this.data.map(e => {
            if (e === 'All') {
              this.isAllSelected = true;
            }

            this.notificationType = this.notificationType.map(a => {
              if (a.value === e) {
                a.active = true;
              }

              return a;
            });

            this.tags = this.tags.map(a => {
              if (a.value === e) {
                a.active = true;
              }

              return a;
            });
          });
        });
  }

  UpdateNotifications() {
    this.dialogRef.close(this.notification);
  }

  updateData(element) {
    if (element.value === 'All') {
      this.isAllSelected = element.active;
    }

    if (element.active) {
      this.notification.push(element.value);
    } else {
      const index = this.notification.indexOf(element.value);
      this.notification.splice(index, 1);
    }
  }

}


/***************************************************************** */

@Component({
  selector: 'app-user-company-alerts',
  templateUrl: './user-company-alerts.html',
  styleUrls: ['./user-companies.component.css'],
  providers: [UserCompaniesService]
})
export class AlertComponent implements OnInit {
  alerts = [];
  newAlert: number;

  constructor(
    private dialogRef: MatDialogRef<AlertComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private alertService: UserCompaniesService,
    private snackBar: MatSnackBar
  ) { }


  ngOnInit() {
    this.alerts = this.data.alerts;
  }

  AddAlert() {
    const id = this.data.id;
    const data = {
      alertPrice: this.newAlert
    };

    this.alertService.addAlert(id, data)
      .subscribe(
        res => {
          this.newAlert = null;
          this.alerts.push(res);
          this.snackBar.open('Alert added successfully', 'close', {
            duration: 2000
          });
        }
      );
  }

  DisableAlert(alert, index) {
    const id = alert.id;
    const data = {
      active: false
    };

    this.alertService.updateAlert(id, data)
      .subscribe(
        res => {
          this.alerts[index]['active'] = false;
          this.snackBar.open('Alert disabled successfully', 'close', {
            duration: 2000
          });
        }
      );
  }

  EnableAlert(alert, index) {
    const id = alert.id;
    const data = {
      active: true
    };

    this.alertService.updateAlert(id, data)
      .subscribe(
        res => {
          this.alerts[index]['active'] = true;
          this.snackBar.open('Alert enabled successfully', 'close', {
            duration: 2000
          });
        }
      );
  }

  RemoveAlert(alert, index) {
    const id = alert.id;

    this.alertService.removeAlert(id)
      .subscribe(
        res => {
          this.alerts.splice(index, 1);
          this.snackBar.open('Alert removed successfully', 'close', {
            duration: 2000
          });
        }
      );
  }

}

