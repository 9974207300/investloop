import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class UserCompaniesService {

  accessToken = localStorage.getItem('accessToken');
  userId = localStorage.getItem('userId');

  constructor(
    private http: HttpClient
  ) { }

  getUserCompanies() {
    const url = `/api/users/${this.userId}/userCompanies`;
    const filter = `{
      "include":[
        {"relation": "companies", "scope": {"include": {"relation": "companyAnnouncements", "scope": {"fields": ["companyId"]}}}},
        {"relation": "alerts"}
      ],
      "where" : {"public": "false"}
    }`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('filter', filter)
    });
  }

  removeUserCompany(id) {
    const url = `/api/userCompanies/${id}`;
    return this.http.delete(url);
  }

  addUserCompany(id) {
    const url = `/api/users/${this.userId}/userCompanies`;
    return this.http.post(url, {
      companyId: id
    });
  }

  getPortfolioComapnies() {
    const url = `/api/users/${this.userId}/portfolios`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('filter', '{"fields": {"companyId":true}}')
    });
  }

  logout() {
    const url = `/api/Users/logout`;
    return this.http.post(url, {});
  }

  updateNotifications(id, data) {
    const url = `/api/UserCompanies/${id}`;
    return this.http.patch(url, {
      notify: data
    });
  }

  addAlert(id, data) {
    const url = `/api/UserCompanies/${id}/alerts`;
    return this.http.post(url, data);
  }

  removeAlert(id) {
    const url = `/api/alerts/${id}/`;
    return this.http.delete(url);
  }

  updateAlert(id, data) {
    const url = `/api/alerts/${id}/`;
    return this.http.patch(url, data);
  }

  getTags() {
    const url = `/api/tags`;
    return this.http.get(url);
  }
}
