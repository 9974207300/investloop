import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import { Router } from '@angular/router';

let Role = false;
let isRoleDefined = false;
@Injectable()
export class AuthService implements OnInit {
  isLoggedIn = false;

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ) {
    // if (!isRoleDefined && !isRunning) {
    //   this.getRole(() => { });
    // }
  }

  // store the URL so we can redirect after logging in
  redirectUrl = '';

  ngOnInit() { }

  login(): void {
    this.isLoggedIn = true;
    this.router.navigate([this.redirectUrl]);
  }

  logout(): void {
    const PublicPageDetails = localStorage.getItem('PublicPageDetails');
    localStorage.clear();
    localStorage.setItem('PublicPageDetails', PublicPageDetails);
    this.isLoggedIn = false;
    this.router.navigate(['']);
    location.reload();
  }

  getAuthorizationToken() {
    const token = localStorage.getItem('accessToken') || 'publicapi';
    return token;
  }

  setRole(roles, cb) {
    Role = roles === 'admin' ? true : false;
    isRoleDefined = true;
    cb();
  }

  getRole(cb) {
    const that = this;

    if (isRoleDefined) {
      return cb(Role);
    }

    const id = localStorage.getItem('userId');
    if (!id) {
      return cb(false);
    }

    const url = `/api/users/${id}/role`;
    this.http.get(url)
      .subscribe(result => {
        this.setRole(result['role'], function () {
          return cb(Role);
        });
      },
      error => {
        return cb(false);
      });
  }

}
