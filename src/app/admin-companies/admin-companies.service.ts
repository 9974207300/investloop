import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class AdminCompaniesService {
  accessToken = localStorage.getItem('accessToken');
  userId = localStorage.getItem('userId');

  constructor(private http: HttpClient) { }

  getAdminCompanies() {
    const url = `/api/companies`;
    return this.http.get(url);
  }
}
