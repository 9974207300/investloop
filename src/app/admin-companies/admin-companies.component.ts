import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AdminCompaniesService } from './admin-companies.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-companies',
  templateUrl: './admin-companies.component.html',
  styleUrls: ['./admin-companies.component.css'],
  providers: [AdminCompaniesService]
})
export class AdminCompaniesComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AdminCompaniesComponent>,
    @Inject(MAT_DIALOG_DATA) public userCompanyData: any,
    public companies: AdminCompaniesService,
    private router: Router
  ) { }

  companyName: string;
  adminCompanies: any;
  allCompanies: any;

  addedCompany = this.userCompanyData.map((result) => {
    return result.companyId;
  });

  ngOnInit() {
    this.companies.getAdminCompanies()
      .subscribe(res => {
        this.adminCompanies = res;
        this.allCompanies = this.adminCompanies.map((result) => {
          if (this.addedCompany.indexOf(result.id) === -1) {
            return result;
          }
          return;
        })
          .filter((company) => {
            return company;
          });
        this.adminCompanies = null;
      });
  }

  getCompany() {
    if (this.companyName.length > 2) {
      this.adminCompanies = this.allCompanies.map((company) => {
        if (company.name.toLowerCase().startsWith(this.companyName.toLowerCase())) {
          return company;
        }
        return;
      });
    } else {
      this.adminCompanies = null;
    }
  }

}
