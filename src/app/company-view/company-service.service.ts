import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class CompanyServiceService {

  constructor(private http: HttpClient) { }

  getAnnouncements(symbol) {
    const url = `/api/companyannouncements/${symbol}/announcements`;
    return this.http.get(url);
  }

}
