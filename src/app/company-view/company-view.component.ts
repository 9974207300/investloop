import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MenuComponent } from '../menu/menu.component';
import * as moment from 'moment';

import { CompanyServiceService } from './company-service.service';

declare const TradingView: any;

@Component({
  selector: 'app-company-view',
  templateUrl: './company-view.component.html',
  styleUrls: ['./company-view.component.css'],
  providers: [CompanyServiceService]
})
export class CompanyViewComponent implements OnInit, AfterViewInit {
  symbol: string;

  widgetData = {
    'width': '100%',
    'height': 610,
    'symbol': '',
    'interval': 'D',
    'timezone': 'IST',
    'theme': 'Light',
    'style': '1',
    'locale': 'in',
    'toolbar_bg': '#f1f3f6',
    'enable_publishing': false,
    'allow_symbol_change': false,
    'details': true,
    'calendar': true,
    'news': [
      'stocktwits',
      'headlines'
    ],
    'container_id': 'tradingview_52ec5'
  };

  announcements = [];
  companyId: string;
  constructor(
    private activeRoute: ActivatedRoute,
    private companyService: CompanyServiceService
  ) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.symbol = params['id'];
    });

    this.widgetData.symbol = `BSE:${this.symbol}`;

    this.companyService.getAnnouncements(this.symbol)
      .subscribe(res => {
        this.companyId = res['data']['id'];
        this.announcements = res['data']['companyAnnouncements'].map(data => {
          data.announceTime = moment(data.announceTime).format('DD MMM Y HH:mm');
          return data;
        });
      });
  }

  ngAfterViewInit() {
    new TradingView.widget(this.widgetData); // tslint:disable-line
  }

}
