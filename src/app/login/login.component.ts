import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, Validators } from '@angular/forms';
import { locale } from 'moment';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort } from '@angular/material';

import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MenuComponent } from '../menu/menu.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // @ViewChild('profileImage') profileImage;

  hide = true;
  loader = false;
  emailExists = '';

  email: string;
  password: string;
  firstName: string;
  lastName: string;
  // profile: any;
  // imageSrc = './../assets/images/profile-default.png';
  isLogin = this.router.url === '/login';

  passwordValidation = new FormControl('', [Validators.required]);
  emailValidation = new FormControl('', [Validators.required, Validators.email]);
  firstValidation = new FormControl('', [Validators.required]);
  lastValidation = new FormControl('', [Validators.required]);

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.snackBar.dismiss();
    const accessToken = localStorage.getItem('accessToken');
    const userId = localStorage.getItem('userId');

    if (userId && accessToken) {
      this.auth.login();
    }
  }

  getEmailErrorMessage() {
    return this.emailValidation.hasError('required') ? 'Email is required' :
      this.emailValidation.hasError('email') ? 'Not a valid email' :
        '';
  }

  verifyUser() {
    const url = `/api/users/${this.email}/isVerified`;

    return this.http.get(url);
  }

  login = () => {
    this.snackBar.dismiss();
    if (this.emailExists === 'true') {
      this.verifyUser()
        .subscribe(
          result => {
            if (result['verified']) {
              this.loginUser();
            } else {
              const snackbar = this.snackBar.open('Please verify your account. Check you mail.', 'Close');
              // snackbar.afterDismissed().subscribe(data => this.router.navigate(['']));
            }
          });
    } else {
      const snackbar = this.snackBar.open('EMAIL DOES NOT EXISTS', 'Close');
    }
  }

  loginUser = () => {
    const body = {
      email: this.email,
      password: this.password
    };

    const url = '/api/users/login';
    this.http.post(url, body, {
      params: new HttpParams()
        .set('include', 'user')
    })
      .subscribe(user => {
        const accessToken = user['id'];
        const userId = user['userId'];
        if (accessToken && userId) {
          const that = this;
          this.auth.setRole(user['role'], function () {
            const username = `${user['user']['first_name']} ${user['user']['last_name']}`;
            that.auth.login();
            localStorage.setItem('username', username);
            localStorage.setItem('accessToken', accessToken);
            localStorage.setItem('userId', userId);
          });
        }
      },
        error => {
          const snackBar = this.snackBar.open('Invalid Credentails', 'Close');
          // const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
          //   width: '300px',
          //   data: { dialog: 'invalid credentials' },
          //   disableClose: true,
          //   hasBackdrop: true
          // });

          // dialogRef.afterClosed();

        });
  }

  reset = (): void => {
    this.password = '';
    this.email = '';
  }

  checkEmail() {
    console.log('check', this.getEmailErrorMessage());
    if (this.getEmailErrorMessage() === '') {
      const url = `/api/users/${this.email}/emailExists`;
      this.http.get(url)
        .subscribe(result => {
          this.emailExists = result['exists'];
        });
    }
  }

  Signup = () => {
    this.snackBar.dismiss();
    this.loader = true;
    const body = {
      username: this.email,
      email: this.email,
      password: this.password,
      first_name: this.firstName,
      last_name: this.lastName,
      // profile: this.profileImage.nativeElement.files[0] ? this.profileImage.nativeElement.files[0] : null
    };

    const url = `/api/Users`;
    this.http.post(url, body)
      .subscribe(user => {
        this.loader = false;
        const snackBar = this.snackBar.open('Email Verification link sent. PLease check you email.', 'Close');
        snackBar.afterDismissed().subscribe(data => this.router.navigate(['']));
        // const dialogRef = this.dialog.open(VerificationEmailComponent, {
        //   data: 'Email Verification link sent. PLease check you email.'
        // });

        // dialogRef.afterClosed()
        //   .subscribe(
        //   result => {
        //     this.router.navigate(['']);
        //   }
        //   );
      });
  }

  ForgotPassword() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { dialog: 'reset-password-email' }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const url = `/api/Users/reset`;
          this.http.post(url, {
            'email': result
          })
            .subscribe();
        }
      });
  }
}

/***************************************************************** */

// @Component({
//   selector: 'app-user-company-form',
//   templateUrl: './verification.html',
//   styleUrls: [],
//   providers: []
// })
// export class VerificationEmailComponent implements OnInit {

//   message = '';

//   constructor(
//     public dialogRef: MatDialogRef<VerificationEmailComponent>,
//     @Inject(MAT_DIALOG_DATA) public data: any
//   ) {
//     this.message = data;
//   }


//   ngOnInit() { }

// }
