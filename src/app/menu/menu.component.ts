import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from '../auth.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [AuthService]
})
export class MenuComponent implements OnInit {

  @Input('page') page: string; // tslint:disable-line
  @Input('loader') loader: boolean; // tslint:disable-line

  isLoggedIn: Boolean;
  userId = localStorage.getItem('userId');
  accessToken = localStorage.getItem('accessToken');
  username = localStorage.getItem('username');
  role: string;

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private appComponent: AppComponent
  ) {
    this.isLoggedIn = this.accessToken !== null && this.userId !== null;
  }

  ngOnInit() {
    this.authService.getRole((roles) => {
      this.role = roles ? 'Admin' : 'User';
    });
  }

  Logout() {
    const url = `/api/Users/logout`;
    return this.http.post(url, {})
      .subscribe(() => {
        this.isLoggedIn = false;
        this.authService.logout();
      });
  }

  Subscribe() {
    this.appComponent.SubscriberModal();
  }

  changePassword() {
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '450px',
      disableClose: true,
      hasBackdrop: true
    });

    dialogRef.afterClosed()
      .subscribe(
      result => {
        if (result) {
          const url = '/api/users/change-password';
          this.http.post(url, {
            oldPassword: result.old,
            newPassword: result.new
          })
            .subscribe(
            results => {
              this.snackBar.open('Password Updated successfully', 'close', {
                duration: 2000
              });
            });
        }
      }
      );
  }
}

/***************************************************************** */

@Component({
  selector: 'app-change-password-form',
  templateUrl: './change-password.html',
  providers: []
})
export class ChangePasswordComponent implements OnInit {
  oldValidation = new FormControl('', [Validators.required]);
  newValidation = new FormControl('', [Validators.required]);
  hide = true;
  oldPassword = '';
  newPassword = '';

  constructor(
    public dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }


  ngOnInit() { }

  ChangePassword() {
    this.dialogRef.close({
      old: this.oldPassword,
      new: this.newPassword
    });
  }

  close() {
    this.dialogRef.close();
  }

}

