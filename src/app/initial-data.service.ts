import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

let role = false;

@Injectable()
export class InitialDataResolver implements Resolve<Observable<any>> {
  role = '';

  constructor(
    private http: HttpClient
  ) { }

  resolve() {
    const accessToken = localStorage.getItem('accessToken');
    const userId = localStorage.getItem('userId');

    if (accessToken && userId && !role) {
      role = true;
      const url = `/api/users/${userId}/role`;
      return this.http.get(url, {
        params: new HttpParams()
        .set('access_token', accessToken)
      });
    } else {
      return null;
    }
  }
}
