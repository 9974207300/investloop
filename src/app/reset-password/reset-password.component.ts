import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { MenuComponent } from '../menu/menu.component';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  password: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    const token = this.activatedRoute.snapshot.queryParams['token'];
      if (!token) {
        this.router.navigate(['']);
      } else {
        localStorage.setItem('accessToken', token);
      }
  }

  Reset() {
    const url = `/api/users/reset-password`;
    this.http.post(url, {
      newPassword: this.password
    })
    .subscribe(
      result => this.router.navigate(['/login']),
      error => console.log('reset password error', error)
    );

  }

}
