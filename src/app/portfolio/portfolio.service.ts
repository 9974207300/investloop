import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class PortfolioService {

  accessToken = localStorage.getItem('accessToken');
  userId = localStorage.getItem('userId');

  constructor(private http: HttpClient) { }

  getUserPorfolio() {
    const url = `/api/users/${this.userId}/portfolios`;
    return this.http.get(url, {
      params: new HttpParams()
      .set('filter', '{"include": {"relation": "companies"},  "order" : "companyId ASC"}')
    });
  }

  createUserPortfolio(data) {
    const url = `/api/users/${this.userId}/portfolios`;
    return this.http.post(url, data);
  }

  deleteUserPortfolio(id) {
    const url = `/api/portfolios/${id}`;
    return this.http.delete(url);
  }

  editUserPortfolio(id, data) {
    const url = `/api/portfolios/${id}`;
    return this.http.patch(url, data);
  }

  getCompanyNetShares(company) {
    const url = `/api/users/${this.userId}/portfolios/${company}/totalShares`;
    return this.http.get(url);
  }
}
