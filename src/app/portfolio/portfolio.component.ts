import { Component, OnInit, Inject } from '@angular/core';
import { PortfolioService } from './portfolio.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { AdminCompaniesService } from '../admin-companies/admin-companies.service';
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material';
import { MenuComponent } from '../menu/menu.component';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

const color = ['#007700', '#E74C3C'];

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css'],
  providers: [PortfolioService]
})
export class PortfolioComponent implements OnInit {
  expanded = '';
  loader = false;

  userPortfolio: any;
  portfolio: any = [];
  companyProfile: any = [];

  totalInvestment = 0;
  totalRecovery = 0;
  totalChange = 0;
  totalColor = color[0];

  constructor(
    private service: PortfolioService,
    private router: Router,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {

    const userId = localStorage.getItem('userId');
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken || !userId) {
      this.router.navigate(['/login']);
    }

  }

  arr: any = [];
  key = '';
  modifydata() {
    this.portfolio = [];
    this.companyProfile = [];
    this.userPortfolio
      .map((data, i) => {
        if (this.key === '') {
          this.key = data.companyId;
          this.arr.push(data);
        } else if (this.key === data.companyId) {
          this.arr.push(data);
        } else {
          this.portfolio.push(this.arr);
          this.arr = [];
          this.arr.push(data);
          this.key = data.companyId;
        }

        if (i === this.userPortfolio.length - 1) {
          this.portfolio.push(this.arr);
        }
      });

    this.portfolio.map(data => {
      const profile = {
        name: data[0].companies.name,
        price: data[0].companies.price,
        netShares: 0,
        netPrice: 0,
        change: 0,
        color: color[0],
        net: 0
      };

      data
        .sort((a, b) => {
          if (a.transaction_date > b.transaction_date) {
            return -1;
          } else {
            return 1;
          }
        })
        .map((element, i) => {
          if (element.buy) {
            profile.netShares = profile.netShares + element.transaction_shares;
            profile.netPrice = (profile.netPrice + element.transaction_price * element.transaction_shares);
          } else {
            profile.netShares = profile.netShares - element.transaction_shares;
            profile.netPrice = (profile.netPrice - element.transaction_price * element.transaction_shares);
          }

          if (i === (data.length - 1)) {
            this.totalInvestment = this.totalInvestment + profile.netPrice;
            this.totalRecovery = this.totalRecovery + (profile.netShares * profile.price);
            profile.netPrice = Math.abs(profile.netPrice / profile.netShares) || 0;
            profile.net = (profile.netShares * profile.price) - (profile.netShares * profile.netPrice);
            profile.change = profile.net / Math.abs(profile.netShares * profile.netPrice) || 0;
            if (profile.change < 0) {
              profile.net = profile.net * -1;
              profile.change = profile.change * -1;
              profile.color = color[1];
            }
            this.companyProfile.push(profile);
          } else {
            const x = element.transaction_price * element.transaction_shares;
            const y = data[i + 1].transaction_price * data[i + 1].transaction_shares;
            element.difference = (x - y) / y;
            if (element.difference < 0) {
              element.difference = element.difference * -1;
              element.color = color[1];
            } else {
              element.color = color[0];
            }
          }

          return element;
        });


      this.totalChange = (this.totalRecovery - this.totalInvestment) / Math.abs(this.totalInvestment) || 0;
      if (this.totalChange < 0) {
        this.totalChange = this.totalChange * -1;
        this.totalColor = color[1];
      } else {
        this.totalColor = color[0];
      }
    });
  }

  getPortfolio() {
    this.service.getUserPorfolio()
      .subscribe(
      data => {
        this.userPortfolio = data;
        this.arr = [];
        this.key = '';
        this.totalInvestment = 0;
        this.totalRecovery = 0;
        this.totalChange = 0;
        this.loader = false;
        this.modifydata();
      });
  }

  ngOnInit() {
    this.getPortfolio();
  }

  addPortfolio() {
    const dialogRef = this.dialog.open(PortfolioFormComponent, {
      width: '600px'
    });
    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.loader = true;
          this.service.createUserPortfolio(result)
            .subscribe(
            data => {
              this.getPortfolio();
              this.snackBar.open('Portfolio added successfully', 'close', {
                duration: 2000
              });
            });
        }
      });
  }

  deletePortfolio(data, name) {
    this.expanded = name;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: { dialog: 'delete' },
      disableClose: true,
      hasBackdrop: true
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.loader = true;
          this.service.deleteUserPortfolio(data.id)
            .subscribe(
            results => {
              this.getPortfolio();
              this.snackBar.open('Portfolio deleted successfully', 'close', {
                duration: 2000
              });
            });
        }
      });
  }

  editPortfolio(data, name) {
    this.expanded = name;
    const dialogRef = this.dialog.open(PortfolioFormComponent, {
      width: '600px',
      data: data
    });

    dialogRef.afterClosed()
      .subscribe(
      result => {
        if (result) {
          this.loader = true;
          this.service.editUserPortfolio(data.id, result)
            .subscribe(
            res => {
              this.getPortfolio();
              this.snackBar.open('Portfolio updated successfully', 'close', {
                duration: 2000
              });
            });
        }
      });
  }
}

/***************************************************************** */

@Component({
  selector: 'app-portfolio-form',
  templateUrl: './portfolio-form.html',
  styleUrls: ['./portfolio.component.css'],
  providers: [AdminCompaniesService, PortfolioService]
})
export class PortfolioFormComponent implements OnInit {
  today = moment();
  allCompanies: any = [];

  filteredStates: any;
  netShares = 0;
  minAllowedShares = 1;
  maxAllowedShares = 100000000000;
  disableSell = false;
  editMode = false;

  dateValidation = new FormControl('', [Validators.required]);
  priceValidation = new FormControl('', [Validators.required]);
  sharesValidation = new FormControl('', [Validators.required]);
  stateCtrl = new FormControl('', []);

  form = {
    buy: true,
    companyId: '',
    transaction_price: '',
    transaction_shares: 1,
    transaction_date: ''
  };

  constructor(
    public dialogRef: MatDialogRef<PortfolioFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public adminService: AdminCompaniesService,
    private router: Router,
    private service: PortfolioService
  ) {

    if (data) {
      this.editMode = true;
      this.form['buy'] = data.buy;
      this.form['companyId'] = data.companies.name;
      this.form['transaction_shares'] = data.transaction_shares;
      this.form['transaction_price'] = data.transaction_price;
      this.form['transaction_date'] = moment(data.transaction_date).format();
      this.companyNetShares();
    } else {
      this.stateCtrl = new FormControl('', [Validators.required]);
    }

    this.filteredStates = this.stateCtrl.valueChanges
      .pipe(
      startWith(''),
      map(state => state ? this.filterStates(state) : this.allCompanies.slice())
      );
  }

  filterStates(name: string) {
    return this.allCompanies.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  ngOnInit() {
    this.adminService.getAdminCompanies()
      .subscribe(
      data => this.allCompanies = data);
  }

  checkEligibleShares() {
    if (this.form['buy']) {
      this.maxAllowedShares = 100000000000;
    } else {
      this.maxAllowedShares = this.netShares;
      this.checkValue('', this.form['transaction_shares']);
    }
  }

  checkValue(event, value) {
    const res = event ? event.target.value : value;
    if (res > this.maxAllowedShares) {
      this.form['transaction_shares'] = this.maxAllowedShares;
    }
    if (res < this.minAllowedShares) {
      this.form['transaction_shares'] = this.minAllowedShares;
    }
  }

  companyNetShares() {
    if (this.form['companyId']) {
      this.service.getCompanyNetShares(this.form['companyId'])
        .subscribe(
        result => {
          this.netShares = result['shares'];

          if (this.editMode) {
            if (this.form['buy']) {
              this.maxAllowedShares = 100000000000;
              this.minAllowedShares = this.form['transaction_shares'] - this.netShares;
              this.minAllowedShares = this.minAllowedShares === 0 ? 1 : this.minAllowedShares;
            } else {
              this.minAllowedShares = 1;
              this.maxAllowedShares = this.netShares + this.form['transaction_shares'];
            }
          } else {
            this.disableSell = this.netShares === 0 ? true : false;
            this.form['buy'] = this.disableSell ? true : this.form['buy'];
            this.checkEligibleShares();
          }

        }
        );
    }
  }

  submitForm() {
    if (this.form['transaction_shares'] === 0) {
      this.close();
    } else {
      const dt = moment(this.form['transaction_date']).startOf('day');
      this.form['transaction_date'] = moment.utc(dt)
        .add(moment().minute(), 'm')
        .add(moment().hours(), 'h')
        .add(moment().seconds(), 's')
        .format();

      const company_name = this.form['companyId'];
      this.allCompanies.filter(company => {
        if (company.name === company_name) {
          this.form['companyId'] = company.id;
          this.dialogRef.close(this.form);
        }
      });
    }
  }

  close() {
    this.dialogRef.close();
  }
}
