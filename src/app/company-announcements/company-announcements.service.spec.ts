import { TestBed, inject } from '@angular/core/testing';

import { CompanyAnnouncementsService } from './company-announcements.service';

describe('CompanyAnnouncementsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyAnnouncementsService]
    });
  });

  it('should be created', inject([CompanyAnnouncementsService], (service: CompanyAnnouncementsService) => {
    expect(service).toBeTruthy();
  }));
});
