import { Component, OnInit } from '@angular/core';
import { CompanyAnnouncementsService } from './company-announcements.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as moment from 'moment';
import { MenuComponent } from '../menu/menu.component';

let lastVisited = {};
const publicPageDetails = {
  lastVisited: '',
  announcements: {},
  reports: {}
};

@Component({
  selector: 'app-company-announcements',
  templateUrl: './company-announcements.component.html',
  styleUrls: ['./company-announcements.component.css'],
  providers: [CompanyAnnouncementsService]
})
export class CompanyAnnouncementsComponent implements OnInit {
  announcements: any;
  companyId: string;
  company: any;

  pageLength: number;
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 50, 100];

  constructor(
    private service: CompanyAnnouncementsService,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    lastVisited = JSON.parse(localStorage.getItem('PublicPageDetails')) || publicPageDetails;
    this.activeRoute.params.subscribe((params: Params) => {
      this.companyId = params['companyId'];
    });

    if (this.companyId) {
      this.service.getCompany(this.companyId)
        .subscribe((companys) => {
          this.company = companys;
          this.company['priceChange'] = 0;
          this.company['priceInc'] = true;
          this.company['time'] = moment().format();
          this.company['color'] = 'gold';

          const data = {
            companyId: this.companyId,
            skip: 0,
            limit: 11,
            isFirst: true
          };

          this.Announcements(data);
        });

      this.service.getCompanyAnnouncementCount(this.companyId)
        .subscribe(
        result => {
          this.pageLength = result['count'];
          lastVisited['announcements'][this.companyId] = this.pageLength;
          localStorage.setItem('PublicPageDetails', JSON.stringify(lastVisited));
        });
    }
  }

  Announcements(data) {
    this.service.getCompanyAnnouncements(data)
      .subscribe((info) => {
        this.announcements = info;

        // this.announcements.sort((a, b) => {
        //   const dt1 = moment(a.announceTime, ['Y/MM/DD', 'Y/MM/DD HH:mm']).format();
        //   const dt2 = moment(b.announceTime, ['Y/MM/DD', 'Y/MM/DD HH:mm']).format();
        //   if (dt1 > dt2) {
        //     return -1;
        //   } else if (dt2 > dt1) {
        //     return 1;
        //   } else {
        //     const dt3 = moment(a.createTime, ['Y/MM/DD', 'Y/MM/DD HH:mm']).format();
        //     const dt4 = moment(b.createTime, ['Y/MM/DD', 'Y/MM/DD HH:mm']).format();
        //     if (dt3 > dt4) {
        //       return -1;
        //     } else if (dt3 > dt4) {
        //       return 1;
        //     } else {
        //       return 0;
        //     }
        //   }
        // });

        if (this.announcements.length > 0 && data.isFirst) {
          this.company['priceChange'] = (this.company.price - this.announcements[0].announcePrice) / this.announcements[0].announcePrice;
          if (this.company.priceChange < 0) {
            this.company['priceChange'] = this.company['priceChange'] * -1;
            this.company['priceInc'] = false;
          } else {
            this.company['priceInc'] = true;
          }
        }

        this.announcements.map((a, index) => {
          a.tags.splice(0, 2);
          a.tooltipTime = moment(a.announceTime).format('LLLL');
          if (moment().diff(a.announceTime) < 86400000) {
            a.announceTime = moment(a.announceTime).from(moment());
          } else {
            a.announceTime = moment(a.announceTime).format('DD/MM/Y HH:mm');
          }
          if (a.announcePrice && this.announcements[index + 1] && this.announcements[index + 1].announcePrice) {
            a.priceChange = (a.announcePrice - this.announcements[index + 1].announcePrice);
            a.priceChange = a.priceChange / this.announcements[index + 1].announcePrice;

            if (a.priceChange < 0) {
              a.priceInc = false;
              a.priceChange = a.priceChange * -1;
            } else {
              a.priceInc = true;
            }
            return a;

          } else {
            a.priceChange = null;
            a.priceInc = true;
            return a;
          }
        });

      });
  }
  pageChange(page) {
    const data = {
      companyId: this.companyId,
      skip: page.pageIndex * page.pageSize,
      limit: page.pageSize + 1
    };

    this.Announcements(data);
  }

  // back() {
  //   this.locations.back();
  // }
}
