import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class CompanyAnnouncementsService {

  accessToken = localStorage.getItem('accessToken');
  userId = localStorage.getItem('userId');

  constructor(private http: HttpClient) { }

  getCompanyAnnouncementCount(companyId) {
    const loginUser = `{"where" : {"or": [{"source": {"neq": "PORTFOLIO"}}, {"userId": "${this.userId}"}]}}`;
    const guestUser = `{"where" : {"source": {"neq": "PORTFOLIO"}}`;
    const isLoggegIn = (this.accessToken && this.userId) ? loginUser : guestUser;

    const url = `/api/companies/${companyId}/companyAnnouncements/count`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('filter', isLoggegIn)
    });
  }

  getCompanyAnnouncements(data) {
    const loginUser = `{"where" : {"or": [{"source": {"neq": "PORTFOLIO"}}, {"userId": "${this.userId}"}]}, "order": "announceTime desc", "limit":"${data.limit}", "skip":"${data.skip}"}`;
    const guestUser = `{"where" : {"source": {"neq": "PORTFOLIO"}}, "order": "announceTime desc", "limit":"${data.limit}", "skip":"${data.skip}"}`;
    const isLoggegIn = (this.accessToken && this.userId) ? loginUser : guestUser;

    const url = `/api/Companies/${data.companyId}/companyAnnouncements`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('filter', isLoggegIn)
    });
  }

  getCompany(id: string) {
    const url = `/api/companies/${id}`;
    return this.http.get(url);
  }

  getAllVerifiedAnnouncements(data) {
    const filter = `{
                      "where" : {"verified": ${data.verified}},
                      "include": {"relation": "announcementComprehends",
                                  "scope": {"fields": ["id", "keep"]}
                                 },
                      "order": "announceTime desc",
                      "limit": "${data.limit}",
                      "skip": "${data.skip}"
                    }`;

    const url = `/api/companyAnnouncements/`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('filter', filter)
    });
  }

  getAllVerifiedAnnouncementsCount(data) {
    const where = `{"verified": ${data.verified}}`;

    const url = `/api/companyAnnouncements/count`;
    return this.http.get(url, {
      params: new HttpParams()
        .set('where', where)
    });
  }

  markAnnouncementVerified(data) {
    const url = `/api/announcementComprehends/${data.id}`;
    delete data.id;
    return this.http.patch(url, data);
  }

}
