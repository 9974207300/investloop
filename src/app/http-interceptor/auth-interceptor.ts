import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { AuthService } from '../auth.service';
import { Services } from '@angular/core/src/view';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  auth: any;
  constructor(private inj: Injector) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.auth = this.inj.get(AuthService);

    // Get the auth token from the service.
    const authToken = this.auth.getAuthorizationToken();

    // Clone the request and replace the original headers with
    const authReq = req.clone({ setHeaders: { Authorization: authToken } });

    return next.handle(authReq);
  }
}
