import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { AuthService } from '../auth.service';

@Injectable()
export class Handle401Interceptor implements HttpInterceptor {
  auth: any;
  constructor(private inj: Injector) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.auth = this.inj.get(AuthService);

    if (req.url.includes('/login')) {
      return next.handle(req);
    }

    return next.handle(req)
      .do(
      event => { },
      error => {
        if (error.status === 401) {
          this.auth.logout();
        }
      }
      );
  }
}
