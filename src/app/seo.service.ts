import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable()
export class SeoService {

  constructor(private meta: Meta) { }

  updateTags(config) {
    // this.meta.updateTag({ property: 'og:type', content: 'article' });
    // this.meta.updateTag({ property: 'og:title', content: config.title });
    // this.meta.updateTag({ property: 'og:description', content: config.description });

    this.meta.updateTag({
      content: 'article'
    },
      `property='og:type'`
    );

    this.meta.updateTag({
      content: config.title
    },
      `property='og:title'`
    );

    this.meta.updateTag({
      content: config.description
    },
      `property='og:description'`
    );
  }
}
