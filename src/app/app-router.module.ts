import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserCompaniesComponent } from './user-companies/user-companies.component';
import { CompanyAnnouncementsComponent } from './company-announcements/company-announcements.component';
import { PublicComponent } from './public-companies/public.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { AnnouncementVerificationComponent } from './announcement-verification/announcement-verification.component';
import { TagsComponent } from './tags/tags.component';
import { CompanyViewComponent } from './company-view/company-view.component';
import { TwitterComponent } from './twitter/twitter.component';

import { AuthGuardService } from './auth-guard.service';
// import { InitialDataResolver } from './initial-data.service';

const appRoutes: Routes = [
  // Admin routes
  // {
  //   path: 'announcement-verification',
  //   component: AnnouncementVerificationComponent,
  //   canActivate: [AuthGuardService]
  // },

  {
    path: 'tags',
    component: TagsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'twitter',
    component: TwitterComponent,
    canActivate: [AuthGuardService]
  },

  // Non Admin routes
  {
    path: '',
    component: PublicComponent,
    // resolve: {
    //   role: InitialDataResolver
    // }
  },
  {
    path: 'my-company',
    component: UserCompaniesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: LoginComponent
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'portfolio',
    component: PortfolioComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'announcements',
    // canActivate: [AuthGuardService],
    children: [
      {
        path: ':companyId',
        component: CompanyAnnouncementsComponent
      }
    ]
  },
  {
    path: 'company',
    children: [
      {
        path: ':id',
        component: CompanyViewComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: ''
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
